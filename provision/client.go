// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package provision

import (
	"github.com/hashicorp/vault/api"
	"github.com/pkg/errors"
	"strconv"
)

func NewClient(inputs map[string]string) (*api.Client, error) {
	address, hasAddress := inputs["address"]
	if !hasAddress {
		return nil, errors.New("Unable to create vault client because address was not configured.")
	}
	token, hasToken := inputs["token"]
	if !hasToken {
		return nil, errors.New("Unable to create vault client because token was not configured.")
	}
	config := &api.Config{Address: address}
	if len(inputs) > 1 {
		tls := &api.TLSConfig{}
		if caCert, ok := inputs["ca_cert"]; ok {
			tls.CACert = caCert
		}
		if caPath, ok := inputs["ca_path"]; ok {
			tls.CAPath = caPath
		}
		if cert, ok := inputs["client_cert"]; ok {
			tls.ClientCert = cert
		}
		if key, ok := inputs["client_key"]; ok {
			tls.ClientKey = key
		}
		if serverName, ok := inputs["tls_server_name"]; ok {
			tls.TLSServerName = serverName
		}
		if insecure, ok := inputs["insecure"]; ok {
			if val, err := strconv.ParseBool(insecure); err != nil {
				tls.Insecure = val
			}
		}
		if err := config.ConfigureTLS(tls); err != nil {
			return nil, errors.Wrap(err, "error during tls configuration")
		}
	}
	client, err := api.NewClient(config)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to create vault client")
	}
	client.SetToken(token)
	return client, nil
}
