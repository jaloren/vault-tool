// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package provision

import (
	"fmt"
	"github.com/hashicorp/hcl2/hcl"
	"github.com/hashicorp/hcl2/hclparse"
	"github.com/spf13/afero"
	"path/filepath"
	"strings"
)

type Parser struct {
	fs afero.Afero
	p  *hclparse.Parser
}

func NewParser(fs afero.Fs) *Parser {
	if fs == nil {
		fs = afero.OsFs{}
	}

	return &Parser{
		fs: afero.Afero{Fs: fs},
		p:  hclparse.NewParser(),
	}
}

func (p *Parser) LoadHCLFile(path string) (hcl.Body, hcl.Diagnostics) {
	src, err := p.fs.ReadFile(path)
	if err != nil {
		return nil, hcl.Diagnostics{
			{
				Severity: hcl.DiagError,
				Summary:  "Failed to read file",
				Detail:   fmt.Sprintf("The file %q could not be read.", path),
			},
		}
	}

	var file *hcl.File
	var diagnostics hcl.Diagnostics
	switch {
	case strings.HasSuffix(path, ".json"):
		file, diagnostics = p.p.ParseJSON(src, path)
	default:
		file, diagnostics = p.p.ParseHCL(src, path)
	}

	if file == nil || file.Body == nil {
		return hcl.EmptyBody(), diagnostics
	}

	return file.Body, diagnostics
}

func (p *Parser) Sources() map[string][]byte {
	return p.p.Sources()
}

// file

func (p *Parser) LoadChangeLogFile(path string, ctx *hcl.EvalContext) (*ChangeLog, hcl.Diagnostics) {
	return p.loadChangeLogFile(path, ctx)
}

func (p *Parser) loadChangeLogFile(path string, ctx *hcl.EvalContext) (*ChangeLog, hcl.Diagnostics) {
	body, diagnostics := p.LoadHCLFile(path)
	if body == nil {
		return nil, diagnostics
	}

	changeLog, changeLogDiagnostics := decodeChangeLogBody(body, ctx)
	diagnostics = append(diagnostics, changeLogDiagnostics...)

	return changeLog, diagnostics
}

// directory

func (p *Parser) LoadChangeLogDir(path string, ctx *hcl.EvalContext) (*ChangeLog, hcl.Diagnostics) {
	paths, diagnostics := p.dirFiles(path)
	if diagnostics.HasErrors() {
		return nil, diagnostics
	}

	changeLogs, changeLogDiagnostics := p.loadFiles(paths, ctx)
	diagnostics = append(diagnostics, changeLogDiagnostics...)

	merge := &ChangeLog{ChangeSets: []*ChangeSet{}}
	for _, changeLog := range changeLogs {
		merge.ChangeSets = append(merge.ChangeSets, changeLog.ChangeSets...)
	}

	return merge, diagnostics
}

func (p *Parser) IsChangeLogDir(path string) bool {
	paths, _ := p.dirFiles(path)
	return len(paths) > 0
}

func (p *Parser) loadFiles(paths []string, ctx *hcl.EvalContext) ([]*ChangeLog, hcl.Diagnostics) {
	var changeLogs []*ChangeLog
	var diagnostics hcl.Diagnostics

	for _, path := range paths {
		changeLog, changeLogDiagnostics := p.LoadChangeLogFile(path, ctx)
		diagnostics = append(diagnostics, changeLogDiagnostics...)
		if changeLog != nil {
			changeLogs = append(changeLogs, changeLog)
		}
	}

	return changeLogs, diagnostics
}

func (p *Parser) dirFiles(dir string) (paths []string, diagnostics hcl.Diagnostics) {
	entries, err := p.fs.ReadDir(dir)
	if err != nil {
		diagnostics = append(diagnostics, &hcl.Diagnostic{
			Severity: hcl.DiagError,
			Summary:  "Failed to read migration directory",
			Detail:   fmt.Sprintf("Migration directory %s does not exist or cannot be read.", dir),
		})
		return
	}

	for _, entry := range entries {
		if entry.IsDir() {
			continue
		}

		name := entry.Name()
		ext := fileExt(name)
		if ext == "" || strings.HasPrefix(name, ".") {
			continue
		}

		path := filepath.Join(dir, name)
		paths = append(paths, path)
	}

	return
}

func fileExt(path string) string {
	if strings.HasSuffix(path, ".hcl") {
		return ".hcl"
	} else if strings.HasSuffix(path, ".json") {
		return ".json"
	} else {
		return ""
	}
}
