// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package provision

import (
	"fmt"
	"github.com/hashicorp/hcl2/gohcl"
	"github.com/hashicorp/hcl2/hcl"
	"github.com/hashicorp/vault/api"
	"github.com/mitchellh/cli"
)

type MountTuneCommand struct {
	*MountConfig
	Path string `hcl:"path,label"`
}

func (c *MountTuneCommand) Run(client *api.Client, ui cli.Ui) error {
	options := api.MountConfigInput{
		Options:                   c.Options,
		DefaultLeaseTTL:           c.DefaultLeaseTTL,
		MaxLeaseTTL:               c.MaxLeaseTTL,
		ForceNoCache:              c.ForceNoCache,
		PluginName:                c.PluginName,
		AuditNonHMACRequestKeys:   c.AuditNonHMACRequestKeys,
		AuditNonHMACResponseKeys:  c.AuditNonHMACResponseKeys,
		ListingVisibility:         c.ListingVisibility,
		PassthroughRequestHeaders: c.PassthroughRequestHeaders,
	}
	ui.Info(fmt.Sprintf("Running `vault mount-tune` path=%s", c.Path))
	if err := client.Sys().TuneMount(c.Path, options); err != nil {
		return err
	}
	ui.Info(fmt.Sprintf("Successfully ran `vault mount-tune` path=%s", c.Path))
	return nil
}

func decodeMountTuneBlock(block *hcl.Block, ctx *hcl.EvalContext) (Change, hcl.Diagnostics) {
	command := &MountTuneCommand{Path: block.Labels[0], MountConfig: &MountConfig{}}
	diagnostics := gohcl.DecodeBody(block.Body, ctx, command.MountConfig)
	return command, diagnostics
}
