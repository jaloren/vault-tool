// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package provision

import (
	"fmt"
	"github.com/hashicorp/hcl2/hcl"
	"github.com/zclconf/go-cty/cty"
)

func decodeValue(value cty.Value, ctx *hcl.EvalContext) (interface{}, hcl.Diagnostics) {
	ty := value.Type()
	if ty.IsPrimitiveType() {
		return decodePrimitive(value, ctx)
	}
	if ty.IsCollectionType() {
		return decodeCollection(value, ctx)
	}
	if ty.IsObjectType() {
		return decodeObject(value, ctx)
	}
	if ty.IsTupleType() {
		return decodeList(value, ctx)
	}
	panic(fmt.Sprintf("unknown type: %v", ty))
}

func decodeObject(value cty.Value, ctx *hcl.EvalContext) (map[string]interface{}, hcl.Diagnostics) {
	var diagnostics hcl.Diagnostics
	kv := make(map[string]interface{})
	for k, v := range value.AsValueMap() {
		value, valueDiagnostics := decodeValue(v, ctx)
		kv[k] = value
		diagnostics = append(diagnostics, valueDiagnostics...)
	}
	return kv, diagnostics
}

func decodePrimitive(value cty.Value, ctx *hcl.EvalContext) (interface{}, hcl.Diagnostics) {
	var diagnostics hcl.Diagnostics
	ty := value.Type()
	switch ty {
	case cty.String:
		return value.AsString(), diagnostics
	case cty.Number:
		return value.AsBigFloat(), diagnostics
	case cty.Bool:
		return value.True(), diagnostics
	}
	diagnostic := &hcl.Diagnostic{
		Severity: hcl.DiagError,
		Summary:  "not a primitive type",
		Detail:   fmt.Sprintf("expected a primitive type but got: %+v", ty),
	}
	diagnostics = append(diagnostics, diagnostic)
	return nil, diagnostics
}

func decodeCollection(value cty.Value, ctx *hcl.EvalContext) (interface{}, hcl.Diagnostics) {
	ty := value.Type()
	if ty.IsListType() || ty.IsSetType() {
		return decodeList(value, ctx)
	}
	if ty.IsMapType() {
		return decodeObject(value, ctx)
	}
	diagnostic := &hcl.Diagnostic{
		Severity: hcl.DiagError,
		Summary:  "not a collection",
		Detail:   fmt.Sprintf("expected a collection type but got: %+v", ty),
	}
	return nil, []*hcl.Diagnostic{diagnostic}
}

func decodeList(value cty.Value, ctx *hcl.EvalContext) ([]interface{}, hcl.Diagnostics) {
	var diagnostics hcl.Diagnostics
	var values []interface{}
	value.ForEachElement(func(key cty.Value, val cty.Value) (stop bool) {
		value, valueDiagnostics := decodeValue(val, ctx)
		values = append(values, value)
		diagnostics = append(diagnostics, valueDiagnostics...)
		return false
	})
	return values, diagnostics
}
