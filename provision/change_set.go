// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package provision

import (
	"fmt"
	"github.com/hashicorp/hcl2/hcl"
	"github.com/hashicorp/vault/api"
	"github.com/mitchellh/cli"
	"github.com/mitchellh/hashstructure"
	"github.com/pkg/errors"
)

// A ChangeSet is a grouping of Change objects that are applied together
type ChangeSet struct {
	Id      string
	Author  string
	Comment string
	Changes []Change
}

// A Change is a change/refactoring to apply to the Vault instance.
type Change interface {
	Run(holder *api.Client, ui cli.Ui) error
}

func (c *ChangeSet) Run(client *api.Client, ui cli.Ui) error {
	var kvPath = "secret/vault-tool/provision"
	secret, err := client.Logical().Read(kvPath)
	if err != nil {
		return errors.Wrapf(err, "could not read from: '%s'", kvPath)
	}

	var data map[string]interface{}
	if secret == nil {
		data = make(map[string]interface{})
	} else {
		data = secret.Data
	}

	hash, err := hashstructure.Hash(c, nil)
	if err != nil {
		return errors.Wrapf(err, "could not checksum change-set: '%s'", c.Id)
	}
	sig := fmt.Sprintf("%X", hash)

	if val, ok := data[c.Id]; ok {
		if sig == val {
			ui.Info(fmt.Sprintf("The change-set %s has already been run previously and will not be run again.", c.Id))
			return nil
		} else {
			return errors.Errorf("The checksum of previously applied change-set has changed; id='%s', old='%s', new='%s'", c.Id, val, sig)
		}
	}

	data[c.Id] = sig

	for _, change := range c.Changes {
		if err := change.Run(client, ui); err != nil {
			return err
		}
	}

	_, err = client.Logical().Write(kvPath, data)
	if err != nil {
		return errors.Wrapf(err, "could not write to: '%s'", kvPath)
	}

	return nil
}

var changeSetSchema = &hcl.BodySchema{
	Attributes: []hcl.AttributeSchema{
		{
			Name:     "author",
			Required: true,
		},
		{
			Name: "comment",
		},
	},
	Blocks: []hcl.BlockHeaderSchema{
		{
			Type:       "audit-enable",
			LabelNames: []string{"path"},
		},
		{
			Type:       "auth-enable",
			LabelNames: []string{"path"},
		},
		{
			Type:       "delete",
			LabelNames: []string{"path"},
		},
		{
			Type:       "mount",
			LabelNames: []string{"path"},
		},
		{
			Type:       "mount-tune",
			LabelNames: []string{"path"},
		},
		{
			Type:       "policy-write",
			LabelNames: []string{"name"},
		},
		{
			Type:       "read",
			LabelNames: []string{"path"},
		},
		{
			Type:       "unmount",
			LabelNames: []string{"path"},
		},
		{
			Type:       "write",
			LabelNames: []string{"path"},
		},
	},
}

func decodeChangeSetBlock(block *hcl.Block, ctx *hcl.EvalContext) (*ChangeSet, hcl.Diagnostics) {
	content, diagnostics := block.Body.Content(changeSetSchema)

	changes := &ChangeSet{
		Id:      block.Labels[0],
		Changes: []Change{},
	}

	for name, attr := range content.Attributes {
		value, valueDiagnostics := attr.Expr.Value(nil)
		diagnostics = append(diagnostics, valueDiagnostics...)
		switch name {
		case "author":
			changes.Author = value.AsString()
			break
		case "comment":
			changes.Comment = value.AsString()
			break
		}
	}

	for _, block := range content.Blocks {
		if decoder, ok := decoders[block.Type]; ok {
			change, diagnostics := decoder(block, ctx)
			changes.Changes = append(changes.Changes, change)
			diagnostics = append(diagnostics, diagnostics...)
		}
	}

	return changes, diagnostics
}

// A function that applies a change/refactoring to the Vault instance
type DecodeChange func(block *hcl.Block, ctx *hcl.EvalContext) (Change, hcl.Diagnostics)

// A mapping from the name of a change to its ApplyChange function
var decoders = map[string]DecodeChange{
	"audit-enable": decodeAuditEnableBlock,
	"auth-enable":  decodeAuthEnableBlock,
	"delete":       decodeDeleteBlock,
	"mount":        decodeMountBlock,
	"mount-tune":   decodeMountTuneBlock,
	"policy-write": decodePolicyWriteBlock,
	"read":         decodeReadBlock,
	"unmount":      decodeUnmountBlock,
	"write":        decodeWriteBlock,
}
