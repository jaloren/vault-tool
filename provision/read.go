// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package provision

import (
	"fmt"
	"github.com/hashicorp/hcl2/gohcl"
	"github.com/hashicorp/hcl2/hcl"
	"github.com/hashicorp/vault/api"
	"github.com/mitchellh/cli"
)

type ReadCommand struct {
	Path   string            `hcl:"path,label"`
	Output map[string]string `hcl:"output,optional"`
	Client map[string]string `hcl:"client,optional"`
}

func (c *ReadCommand) Run(client *api.Client, ui cli.Ui) error {
	target := client
	if c.Client != nil {
		ui.Info(fmt.Sprintf("new client: %+v", c.Client))
		other, err := NewClient(c.Client)
		if err != nil {
			return err
		}
		if other != nil {
			target = other
		}
	}

	ui.Info(fmt.Sprintf("Running `vault read` path=%s", c.Path))
	secret, err := target.Logical().Read(c.Path)
	if err != nil {
		return err
	}
	ui.Info(fmt.Sprintf("Successfully ran `vault read` path=%s", c.Path))

	if secret != nil && c.Output != nil {
		return handleCommandOutputs(client, secret.Data, c.Output)
	} else {
		return nil
	}
}

func decodeReadBlock(block *hcl.Block, ctx *hcl.EvalContext) (Change, hcl.Diagnostics) {
	command := &ReadCommand{Path: block.Labels[0]}
	diagnostics := gohcl.DecodeBody(block.Body, ctx, command)
	return command, diagnostics
}
