// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package provision

import (
	"fmt"
	"github.com/hashicorp/hcl2/hcl"
	"github.com/hashicorp/vault/api"
	"github.com/mitchellh/cli"
	"github.com/pkg/errors"
)

// A ChangeLog is a sequence of ChangeSet objects.
type ChangeLog struct {
	ChangeSets []*ChangeSet
}

func (c *ChangeLog) Run(client *api.Client, ui cli.Ui) error {
	for _, changeSet := range c.ChangeSets {
		ui.Info(fmt.Sprintf("Applying migrations from change-set: %s", changeSet.Id))
		if err := changeSet.Run(client, ui); err != nil {
			return errors.Wrap(err, fmt.Sprintf("Error applying migrations from change-set: %s", changeSet.Id))
		}
		ui.Info(fmt.Sprintf("Successfully applied migrations from change-set: %s", changeSet.Id))
	}
	return nil
}

var changeLogFileSchema = &hcl.BodySchema{
	Blocks: []hcl.BlockHeaderSchema{
		{
			Type:       "change-set",
			LabelNames: []string{"id"},
		},
	},
}

func decodeChangeLogBody(body hcl.Body, ctx *hcl.EvalContext) (*ChangeLog, hcl.Diagnostics) {
	var diagnostics hcl.Diagnostics

	file := &ChangeLog{ChangeSets: []*ChangeSet{}}
	content, contentDiagnostics := body.Content(changeLogFileSchema)
	diagnostics = append(diagnostics, contentDiagnostics...)

	for _, block := range content.Blocks {
		switch block.Type {
		case "change-set":
			changeSet, changeSetDiagnostics := decodeChangeSetBlock(block, ctx)
			diagnostics = append(diagnostics, changeSetDiagnostics...)
			if changeSet != nil {
				file.ChangeSets = append(file.ChangeSets, changeSet)
			}
		}
	}

	return file, diagnostics
}
