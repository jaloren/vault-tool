// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package provision

import (
	"fmt"
	"github.com/hashicorp/hcl2/hcl"
	"github.com/hashicorp/vault/api"
	"github.com/mitchellh/cli"
)

type DeleteCommand struct {
	Path string `hcl:"path,label"`
}

func (c *DeleteCommand) Run(client *api.Client, ui cli.Ui) error {
	ui.Info(fmt.Sprintf("Running `vault delete` path=%s", c.Path))
	if _, err := client.Logical().Delete(c.Path); err != nil {
		return err
	}
	ui.Info(fmt.Sprintf("Successfully ran `vault delete` path=%s", c.Path))
	return nil
}

func decodeDeleteBlock(block *hcl.Block, ctx *hcl.EvalContext) (Change, hcl.Diagnostics) {
	var diagnostics hcl.Diagnostics
	command := &DeleteCommand{Path: block.Labels[0]}
	return command, diagnostics
}
