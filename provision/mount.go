// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package provision

import (
	"fmt"
	"github.com/hashicorp/hcl2/gohcl"
	"github.com/hashicorp/hcl2/hcl"
	"github.com/hashicorp/vault/api"
	"github.com/mitchellh/cli"
)

type MountCommand struct {
	*Mount
	Path string `hcl:"path,label"`
}

type Mount struct {
	Type        string            `hcl:"type,attr"`
	Description string            `hcl:"description,optional"`
	Config      MountConfig       `hcl:"config,block"`
	Options     map[string]string `hcl:"options,optional"`
	Local       bool              `hcl:"local,optional"`
	PluginName  string            `hcl:"plugin_name,optional"`
	SealWrap    bool              `hcl:"seal_wrap,optional"`
}

type MountConfig struct {
	Options                   map[string]string `hcl:"options,optional"`
	DefaultLeaseTTL           string            `hcl:"default_lease_ttl,optional"`
	MaxLeaseTTL               string            `hcl:"max_lease_ttl,optional"`
	ForceNoCache              bool              `hcl:"force_no_cache,optional"`
	PluginName                string            `hcl:"plugin_name,optional"`
	AuditNonHMACRequestKeys   []string          `hcl:"audit_non_hmac_request_keys,optional"`
	AuditNonHMACResponseKeys  []string          `hcl:"audit_non_hmac_response_keys,optional"`
	ListingVisibility         string            `hcl:"listing_visibility,optional"`
	PassthroughRequestHeaders []string          `hcl:"passthrough_request_headers,optional"`
}

func (c *MountCommand) Run(client *api.Client, ui cli.Ui) error {
	options := &api.MountInput{
		Type:        c.Type,
		Description: c.Description,
		Config: api.MountConfigInput{
			Options:                   c.Config.Options,
			DefaultLeaseTTL:           c.Config.DefaultLeaseTTL,
			MaxLeaseTTL:               c.Config.MaxLeaseTTL,
			ForceNoCache:              c.Config.ForceNoCache,
			PluginName:                c.Config.PluginName,
			AuditNonHMACRequestKeys:   c.Config.AuditNonHMACRequestKeys,
			AuditNonHMACResponseKeys:  c.Config.AuditNonHMACResponseKeys,
			ListingVisibility:         c.Config.ListingVisibility,
			PassthroughRequestHeaders: c.Config.PassthroughRequestHeaders,
		},
		Options:    c.Options,
		Local:      c.Local,
		PluginName: c.PluginName,
		SealWrap:   c.SealWrap,
	}
	ui.Info(fmt.Sprintf("Running `vault mount` type=%s path=%s", c.Type, c.Path))
	if err := client.Sys().Mount(c.Path, options); err != nil {
		return err
	}
	ui.Info(fmt.Sprintf("Successfully ran `vault mount` type=%s path=%s", c.Type, c.Path))
	return nil
}

func decodeMountBlock(block *hcl.Block, ctx *hcl.EvalContext) (Change, hcl.Diagnostics) {
	command := &MountCommand{Path: block.Labels[0], Mount: &Mount{Config: MountConfig{}}}
	diagnostics := gohcl.DecodeBody(block.Body, ctx, command.Mount)
	return command, diagnostics
}
