// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package provision

import (
	"encoding/base64"
	"fmt"
	"github.com/hashicorp/hcl2/gohcl"
	"github.com/hashicorp/hcl2/hcl"
	"github.com/hashicorp/vault/api"
	"github.com/mitchellh/cli"
	"github.com/pkg/errors"
	"io/ioutil"
	"strings"
)

type WriteCommand struct {
	Path   string `hcl:"path,label"`
	Data   map[string]interface{}
	Output map[string]string `hcl:"output,optional"`
	Client map[string]string `hcl:"client,optional"`
	Remain hcl.Attributes    `hcl:",remain"`
}

func (c *WriteCommand) Run(client *api.Client, ui cli.Ui) error {
	target := client
	if c.Client != nil {
		ui.Info(fmt.Sprintf("new client: %+v", c.Client))
		other, err := NewClient(c.Client)
		if err != nil {
			return err
		}
		if other != nil {
			target = other
		}
	}

	for k, v := range c.Data {
		if s, ok := v.(string); ok {
			if s[0] == '<' {
				contents, err := ioutil.ReadFile(s[1:])
				if err != nil {
					return errors.Wrapf(err, "error reading file: %s", s[1:])
				}
				s = string(contents)
			}
			if strings.HasPrefix(c.Path, "consul/roles/") && k == "policy" {
				s = base64.StdEncoding.EncodeToString([]byte(s))
			}
			c.Data[k] = s
		}
	}

	ui.Info(fmt.Sprintf("Running `vault write` path=%s", c.Path))
	secret, err := target.Logical().Write(c.Path, c.Data)
	if err != nil {
		ui.Warn(fmt.Sprintf("Error running `vault write` path=%s; data=%v", c.Path, c.Data))
		return err
	}
	ui.Info(fmt.Sprintf("Successfully ran `vault write` path=%s", c.Path))

	if secret != nil && c.Output != nil {
		return handleCommandOutputs(client, secret.Data, c.Output)
	} else {
		return nil
	}
}

func decodeWriteBlock(block *hcl.Block, ctx *hcl.EvalContext) (Change, hcl.Diagnostics) {
	command := &WriteCommand{Path: block.Labels[0]}
	diagnostics := gohcl.DecodeBody(block.Body, ctx, command)
	if data, ok := command.Remain["data"]; ok {
		// because gohcl panics when it sees map[string]interface{}
		value, dataDiagnostics := data.Expr.Value(ctx)
		diagnostics = append(diagnostics, dataDiagnostics...)
		if value.Type().IsObjectType() || value.Type().IsMapType() {
			value, diagnostic := decodeObject(value, ctx)
			command.Data = value
			diagnostics = append(diagnostics, diagnostic...)
		} else {
			diagnostic := &hcl.Diagnostic{
				Severity: hcl.DiagError,
				Summary:  "block 'write' attribute 'data' is neither an object nor a map",
				Detail:   value.GoString(),
			}
			diagnostics = append(diagnostics, diagnostic)
		}
	}
	return command, diagnostics
}
