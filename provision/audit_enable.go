// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package provision

import (
	"fmt"
	"github.com/hashicorp/hcl2/gohcl"
	"github.com/hashicorp/hcl2/hcl"
	"github.com/hashicorp/vault/api"
	"github.com/mitchellh/cli"
)

type AuditEnableCommand struct {
	*AuditEnable
	Path string `hcl:"path,label"`
}

type AuditEnable struct {
	Type        string            `hcl:"type"`
	Description string            `hcl:"description,optional"`
	Options     map[string]string `hcl:"options"`
	Local       bool              `hcl:"local,optional"`
}

func (c *AuditEnableCommand) Run(client *api.Client, ui cli.Ui) error {
	options := &api.EnableAuditOptions{
		Type:        c.Type,
		Description: c.Description,
		Options:     c.Options,
		Local:       c.Local,
	}
	ui.Info(fmt.Sprintf("Running `vault audit-enable` type=%s path=%s", c.Type, c.Path))
	if err := client.Sys().EnableAuditWithOptions(c.Path, options); err != nil {
		return err
	}
	ui.Info(fmt.Sprintf("Successfully ran `vault audit-enable` type=%s path=%s", c.Type, c.Path))
	return nil
}

func decodeAuditEnableBlock(block *hcl.Block, ctx *hcl.EvalContext) (Change, hcl.Diagnostics) {
	command := &AuditEnableCommand{Path: block.Labels[0], AuditEnable: &AuditEnable{}}
	diagnostics := gohcl.DecodeBody(block.Body, ctx, command.AuditEnable)
	return command, diagnostics
}
