// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package provision

import (
	"fmt"
	"github.com/hashicorp/vault/api"
	"github.com/pkg/errors"
	"os"
	"strings"
	"syscall"
)

func handleCommandOutputs(client *api.Client, secrets map[string]interface{}, output map[string]string) error {
	for k, path := range output {
		if secret, ok := secrets[k]; ok {
			if strings.HasPrefix(path, ">") {
				var filemode int
				if strings.HasPrefix(path, ">>") {
					path = path[2:]
					filemode = syscall.O_RDWR | syscall.O_CREAT
				} else {
					path = path[1:]
					filemode = syscall.O_RDWR | syscall.O_CREAT | syscall.O_TRUNC
				}
				_, err := writeToFile(path, filemode, secret)
				if err != nil {
					return err
				}
			} else if strings.HasPrefix(path, "secret/") {
				writeToPath(client, path, map[string]interface{}{k: secret})
			} else {
				return errors.Errorf("Unexpected Output: %v", path)
			}
		} else {
			return errors.Errorf("No such secret %v", k)
		}
	}
	return nil
}

// Write data into a Vault KV store
func writeToPath(client *api.Client, path string, data map[string]interface{}) error {
	secret, err := client.Logical().Read(path)
	if err != nil {
		return nil
	}

	var values map[string]interface{}
	if secret != nil {
		values = secret.Data
		for k, v := range data {
			values[k] = v
		}
	} else {
		values = data
	}

	_, err = client.Logical().Write(path, values)
	if err != nil {
		return err
	}

	return nil
}

// Write data into a file
func writeToFile(filename string, filemode int, data interface{}) (int, error) {
	f, err := os.OpenFile(filename, filemode, 0600)
	if err != nil {
		return 0, err
	}
	defer f.Close()
	return fmt.Fprintf(f, "%v", data)
}
