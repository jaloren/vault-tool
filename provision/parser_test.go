// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package provision

import (
	"os"
	"path"
	"testing"

	"github.com/hashicorp/hcl2/hcl"
	"github.com/spf13/afero"
)

func testParser(files map[string]string) *Parser {
	fs := afero.Afero{Fs: afero.NewMemMapFs()}

	for filePath, contents := range files {
		dirPath := path.Dir(filePath)
		err := fs.MkdirAll(dirPath, os.ModePerm)
		if err != nil {
			panic(err)
		}
		err = fs.WriteFile(filePath, []byte(contents), os.ModePerm)
		if err != nil {
			panic(err)
		}
	}

	return NewParser(fs)
}

func TestSimonIsGreat(t *testing.T) {
	t.Run("demo", func(t *testing.T) {
		p := testParser(map[string]string{
			"some.hcl": example,
		})
		got, diags := p.LoadChangeLogFile("some.hcl", &hcl.EvalContext{})
		if len(diags) > 0 {
			t.Errorf("wrong number of diagnostics %d; want %d", len(diags), 0)
			for _, diag := range diags {
				t.Logf("- %s", diag)
			}
		}
		if len(got.ChangeSets) != 1 {
			t.Errorf("wrong number of change-sets %d; want %d", len(got.ChangeSets), 1)
		}
	})
}

var example = `
change-set "hello" {
  author  = "simon-wenmouth"
  comment = "my first hcl parser"
  audit-enable "file" {
    type = "file"
    options = {
      file_path = "/var/log/vault/vault_audit.log"
    }
  }
  auth-enable "approle" {
    type = "approle"
  }
}
`
