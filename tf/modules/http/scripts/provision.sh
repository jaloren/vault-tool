#!/usr/bin/env bash

set -u
set -o pipefail

function docker_exec() {
    local container_id=$1
    local command=$2
    docker exec "${container_id}" bash -c "${command}"
}

function main() {
    local container_id=$1
    docker_exec "${container_id}" 'vault-tool wait'
    docker_exec "${container_id}" 'vault-tool unsafe initialize'
    docker_exec "${container_id}" 'env VAULT_TOKEN=$(vault-tool unsafe root-token) vault-tool provision'
    return 0
}

main "$1"
