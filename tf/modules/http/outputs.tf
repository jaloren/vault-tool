
output "vault_container_id" {
  value = "${docker_container.vault.id}"
  description = "the vault docker container id"
}

output "vault_root_token" {
  value = "${data.external.initialization.result["root_token"]}"
  description = "the vault root token"
}

output "vault_tool_role_id" {
  value = "${data.external.approle.result["role_id"]}"
  description = "the vault-tool approle role-id"
}

output "vault_tool_secret_id" {
  value = "${data.external.approle.result["secret_id"]}"
  description = "the vault-tool approle secret-id"
}
