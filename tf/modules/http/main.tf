provider "docker" {
}

resource "docker_volume" "vault_volume" {
  name = "vault_volume"
}

resource "docker_container" "vault" {
  name  = "vault"
  image = "simon-wenmouth/vault:0.10.1"

  env = [
    // for `entrypoint`
    "TOOL_TLS_DISABLE_SERVER=true",
    "TOOL_TLS_PROVISION=no",
    // for `healthcheck`
    "VAULT_ADDR=http://localhost:8200",
    // for `provision`
    "TOOL_MIGRATIONS_URL=${var.migrations_url}"
  ]

  hostname      = "vault"
  domainname    = "${var.domain_name}"
  networks      = ["${var.docker_network_name}"]
  network_alias = ["vault", "vault.${var.domain_name}"]

  ports {
    internal = 8200
    external = "${var.vault_port}"
  }

  volumes {
    volume_name    = "${docker_volume.vault_volume.name}"
    container_path = "/opt/vault/data"
  }

  provisioner "local-exec" {
    interpreter = ["bash", "-c"]
    command     = "${path.module}/scripts/provision.sh ${docker_container.vault.id}"
  }
}

data "external" "initialization" {
  program = ["docker", "exec", "${docker_container.vault.id}", "bash", "-c", "jq -cr '. | {root_token: .root_token, unseal_key: .keys[0]}' /opt/vault/data/.vault-unsafe-initialization"]
}

data "external" "approle" {
  program = ["docker", "exec", "${docker_container.vault.id}", "bash", "-c", "env VAULT_TOKEN=$(vault-tool unsafe root-token) vault read -format=json secret/vault-tool/roles/vault-tool | jq -cr '. | {role_id: .data.role_id, secret_id: .data.secret_id}'"]
}
