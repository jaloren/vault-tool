
variable "domain_name" {
  type    = "string"
  default = "wenmouth.test"
}

variable "docker_network_name" {
  type    = "string"
}

variable "docker_registry_name" {
  type    = "string"
}

variable "migrations_url" {
  type    = "string"
  default = "https://bitbucket.org/waterproofed/vault-policies.git//root-trusted-ca"
}

variable "vault_port" {
  type    = "string"
  default = "8200"
}
