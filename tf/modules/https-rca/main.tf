provider "docker" {
}

resource "docker_volume" "trusted_root_ca_volume" {
  name = "trusted_root_ca_volume"
}

resource "docker_container" "trusted_root_ca" {
  name  = "trusted-root-ca"
  image = "simon-wenmouth/vault:0.10.1"

  env = [
    // for `entrypoint`
    "TOOL_TLS_PROVISION=self-signed",
    // for `provision`
    "TOOL_MIGRATIONS_URL=${var.migrations_url}",
    // for `tls`
    "VAULT_CLIENT_CERT=/opt/vault/config/keys/server.cert.pem",
    "VAULT_CLIENT_KEY=/opt/vault/config/keys/server.key.pem",
    "VAULT_CACERT=/opt/vault/config/keys/server.ca.pem"
  ]

  hostname      = "trusted-root-ca"
  domainname    = "${var.domain_name}"
  networks      = ["${var.docker_network_name}"]
  network_alias = ["trusted-root-ca", "trusted-root-ca.${var.domain_name}"]

  ports {
    internal = 8200
    external = "${var.vault_port}"
  }

  volumes {
    volume_name    = "${docker_volume.trusted_root_ca_volume.name}"
    container_path = "/opt/vault/data"
  }

  provisioner "local-exec" {
    interpreter = ["bash", "-c"]
    command     = "${path.module}/scripts/provision.sh ${docker_container.trusted_root_ca.id}"
  }
}

data "external" "initialization" {
  program = ["docker", "exec", "${docker_container.trusted_root_ca.id}", "bash", "-c", "jq -cr '. | {root_token: .root_token, unseal_key: .keys[0]}' /opt/vault/data/.vault-unsafe-initialization"]
}

data "external" "approle" {
  program = ["docker", "exec", "${docker_container.trusted_root_ca.id}", "bash", "-c", "env VAULT_TOKEN=$(vault-tool unsafe root-token) vault read -format=json secret/vault-tool/roles/vault-tool | jq -cr '. | {role_id: .data.role_id, secret_id: .data.secret_id}'"]
}
