
provider "docker" {
}

resource "docker_network" "private_network" {
  name            = "wenmouth.test"
  check_duplicate = true
}

module "fixture" {
  source = "../../modules/http"
  providers = {
    docker = "docker"
  }

  docker_network_name  = "wenmouth.test"
  docker_registry_name = ""
}

output "vault_container_id" {
  value = "${module.fixture.vault_container_id}"
}

output "vault_root_token" {
  value = "${module.fixture.vault_root_token}"
}

output "vault_tool_role_id" {
  value = "${module.fixture.vault_tool_role_id}"
}

output "vault_tool_secret_id" {
  value = "${module.fixture.vault_tool_secret_id}"
}
