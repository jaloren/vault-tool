
.PHONY: bin dev img get fmt test functional vet

default: dev

# build for all architectures
bin:
	@DEV_BUILD=false sh -c "'$(CURDIR)/scripts/build.sh'"

# build for development architecture
dev:
	@DEV_BUILD=true sh -c "'$(CURDIR)/scripts/build.sh'"

# build docker image
img:
	@DEV_BUILD=false XC_OSARCH=linux/amd64 sh -c "'$(CURDIR)/scripts/build.sh'"
	docker build --tag simon-wenmouth/vault:0.10.1 .

# fetch the build tools and vendor files
get:
	go get -u github.com/mitchellh/gox
	go get -u github.com/kardianos/govendor
	govendor sync

# reformat the source code
fmt:
	go fmt $(go list ./... | grep -v /vendor/)

# run the unit tests
test:
	go test $(go list ./... | grep -v /vendor/)

# run the functional tests
functional:
	cd test; go test

# lint the code
vet:
	go vet $(go list ./... | grep -v /vendor/)
