# vault-tool

A tool that aims to simplify the secure provisioning and
configuration of [HashiCorp Vault](https://www.vaultproject.io). 

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)

## Installation

If you wish to work on vault-tool you'll first need Go installed
on your machine.

For local development first make sure Go is properly installed,
including setting up a GOPATH.  Next, clone this repository into
`$GOPATH/src/github.com/simon-wenmouth/vault-tool`. You can then
build the binary as follows:

```bash
make get
make dev
```

you'll find the built artifact, if for the host os/arch will have
been copied into the directories `bin` and `$(GOPATH)/bin`.

if you want to build for a single target os/architecture:

```bash
env XC_OSARCH=linux/amd64 make bin
```

the built artifact is `pkg/${OS}_${ARCH}/vault-tool`

## Usage

Please refer to the help output of the various commands to understand
the command line flags and environment variables that they consume.

### Trusted Root CA

The commands of the tool are usually ran in the following order.

* _entrypoint_ - to configure and start the vault server
* _unsafe initialize_ - to initialize and unseal the vault server
* _unsafe root-token_ - to get the vault root token
* _provision_ - to configure the vault server (using the root token)
* _approle server_ - to provision vault tokens for applications

### Intermediate CA

Having setup a trusted root certificate authority, the typical steps
to configure an intermediate certificate authority are as follows.

* _entrypoint_ - to configure and start the vault server
* _unsafe initialize_ - to initialize and unseal the vault server
* _unsafe root-token_ - to get the vault root token
* _approle exec_ of _provision_ - to configure the vault server
  (using the root token, as well as a token from the _approle server_).

### Examples

#### TLS

First, we bring up a trusted root ca and open a shell. 

```bash
env XC_OSARCH=linux/amd64 make bin
docker build --tag simon-wenmouth/vault:0.10.1 .
docker-compose -f docker-compose.tls.yml up -d trusted-root-ca
docker exec -it vault-tool_trusted-root-ca_1 sh
```

We now initialize and provision the trusted root ca.

```bash
vault-tool unsafe initialize
vault-tool healthcheck
export VAULT_TOKEN=$(vault-tool unsafe root-token)
vault-tool provision
vault-tool approle server -advertise-interface=eth0 &
```

After exiting back to the host, we start the intermediate ca and
open a shell.

```bash
docker-compose -f docker-compose.tls.yml up -d intermediate-ca
docker logs vault-tool_intermediate-ca_1
docker exec -it vault-tool_intermediate-ca_1 sh
```

Let's provision the intermediate also.

```bash
vault-tool unsafe initialize
vault-tool healthcheck
export VAULT_TOKEN=$(vault-tool unsafe root-token)
vault-tool approle exec -approle=vault -approle-server=http://trusted-root-ca.wenmouth.test:8250 -binary=/opt/vault/bin/vault-tool -env-prefix=ROOT_CA -env-variables=TOOL_MIGRATIONS_URL,TOOL_MIGRATIONS_ENV,VAULT_TOKEN --wrap-token=false -- vault-tool provision
```

You now have a provisioned trusted root ca / intermediate ca pair
both with ca certificate. 

You can now tear down the servers.

```bash
docker-compose -f docker-compose.tls.yml down --volumes
```

#### NO TLS

As with TLS, let's build a docker image and open a shell on the
running vault container.

```bash
env XC_OSARCH=linux/amd64 make bin
docker build --tag simon-wenmouth/vault:0.10.1 .
docker-compose -f docker-compose.no-tls.yml up -d
docker exec -it vault-tool_vault_1 sh
```

We can now provision the server.

```bash
vault-tool unsafe initialize
vault-tool healthcheck
export VAULT_TOKEN=$(vault-tool unsafe root-token)
vault-tool provision
```

You can now tear down the servers.

```bash
docker-compose -f docker-compose.no-tls.yml down --volumes
```

## Contributing

See the [CONTRIBUTING](CONTRIBUTING.md) file for details.

## Authors

* **Simon Wenmouth** - *Initial work* - [simon-wenmouth](https://github.com/simon-wenmouth)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE.txt) file for details.
