FROM ubuntu:18.10

LABEL name="Docker Container"
LABEL vendor="Hashicorp, Inc",
LABEL license="Mozilla Public License, version 2.0"
LABEL maintainer="simon-wenmouth@users.noreply.github.com"

ARG KEYSERVER=ha.pool.sks-keyservers.net
ARG VAULT_VERSION=0.10.1
ARG VAULT_ZIP_URL=https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip
ARG VAULT_SIG_URL=https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_SHA256SUMS.sig
ARG VAULT_SHA_URL=https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_SHA256SUMS

ARG VAULT_UID=101
ARG VAULT_GID=101
ARG VAULT_LOGIN=vault

ENV VAULT_HOME=/opt/vault \
    VAULT_VERSION=${VAULT_VERSION}

ENV PATH=${PATH}:${VAULT_HOME}/bin

RUN mkdir --parents "/opt/vault/bin" "/opt/vault/data" "/opt/vault/config/keys" "/var/log/vault"

WORKDIR /opt/vault

RUN set -x \
    && apt-get update --yes \
    && apt-get install --no-install-recommends --yes ca-certificates curl dirmngr gpg gpg-agent jq unzip \
    && gpg --keyserver "${KEYSERVER}" --recv-keys 51852D87348FFC4C \
    && curl -fSL "${VAULT_ZIP_URL}" -o "vault_${VAULT_VERSION}_linux_amd64.zip" \
    && curl -fSL "${VAULT_SHA_URL}" -o "vault_${VAULT_VERSION}.sha" \
    && curl -fSL "${VAULT_SIG_URL}" -o "vault_${VAULT_VERSION}.sha.sig" \
    && gpg --verify "vault_${VAULT_VERSION}.sha.sig" "vault_${VAULT_VERSION}.sha" \
    && grep "vault_${VAULT_VERSION}_linux_amd64.zip" "vault_${VAULT_VERSION}.sha" > "vault_${VAULT_VERSION}_linux_amd64.sha" \
    && sha256sum --check "vault_${VAULT_VERSION}_linux_amd64.sha" \
    && unzip "vault_${VAULT_VERSION}_linux_amd64.zip" -d "/opt/vault/bin/" \
    && rm "vault_${VAULT_VERSION}_linux_amd64.zip" \
    && rm "vault_${VAULT_VERSION}_linux_amd64.sha" \
    && rm "vault_${VAULT_VERSION}.sha" \
    && rm "vault_${VAULT_VERSION}.sha.sig" \
    && addgroup --system --gid "${VAULT_GID}" "${VAULT_LOGIN}" \
    && adduser --system --shell /bin/bash --uid  "${VAULT_UID}" --gid "${VAULT_GID}" "${VAULT_LOGIN}" \
    && chown -R "${VAULT_UID}":"${VAULT_GID}" /opt/vault /var/log/vault \
    && chmod a+x /opt/vault/bin/* \
    && apt-get clean \
    && rm -Rf /var/lib/apt/lists/*

COPY pkg/linux_amd64/vault-tool /opt/vault/bin/

USER ${VAULT_UID}

WORKDIR /opt/vault/

VOLUME /opt/vault/data/

VOLUME /var/log/vault/

# 8200 is for client-server communications
# 8201 is for server-server communications
EXPOSE 8200 8201

HEALTHCHECK --start-period=5s CMD /opt/vault/bin/vault-tool healthcheck

ENTRYPOINT ["/opt/vault/bin/vault-tool"]

CMD ["entrypoint"]
