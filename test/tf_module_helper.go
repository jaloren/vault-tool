// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package test

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"
	"time"

	"github.com/gruntwork-io/terratest/modules/retry"
	"github.com/gruntwork-io/terratest/modules/shell"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/hashicorp/vault/api"
	"github.com/pkg/errors"
)

func testDockerContainer(t *testing.T, setupOptions *terraform.Options) {
	containerId := terraform.OutputRequired(t, setupOptions, "vault_container_id")
	status := shell.RunCommandAndGetOutput(t, shell.Command{
		Command: "docker",
		Args:    []string{"container", "inspect", "-f", "{{.State.Status}}", containerId},
	})
	if status != "running" {
		t.Fatalf("container not running: %v", containerId)
	}
	health := retry.DoWithRetry(t, "container health", 3, 10*time.Second, func() (string, error) {
		output, err := shell.RunCommandAndGetOutputE(t, shell.Command{
			Command: "docker",
			Args:    []string{"container", "inspect", "-f", "{{.State.Health.Status}}", containerId},
		})
		if err != nil {
			return "", err
		}
		if output == "starting" {
			return "", errors.New("container starting")
		}
		return output, nil
	})
	if health != "healthy" {
		t.Fatalf("container not healthy: %v", health)
	}
}

func testVaultHealth(t *testing.T, client *api.Client) {
	response, err := client.Sys().Health()
	if err != nil {
		t.Fatal(errors.Wrap(err, "Failed to retrieve vault health."))
	}
	if !response.Initialized {
		t.Fatal(errors.Wrap(err, "Failed to initialize vault."))
	}
	if response.Sealed {
		t.Fatal(errors.Wrap(err, "Failed to unseal vault."))
	}
}

func testVaultProvision(t *testing.T, client *api.Client) {
	secret, err := client.Logical().Read("secret/vault-tool/provision")
	if err != nil {
		t.Fatal(errors.Wrap(err, "Failed to read path 'secret/vault-tool/provision'."))
	}
	val, ok := secret.Data["root-trusted-ca"]
	if !ok {
		t.Fatal(errors.Wrap(err, "Failed to provision change-set 'root-trusted-ca'."))
	}
	if str, ok := val.(string); str == "" || !ok {
		t.Fatal(errors.Wrap(err, "Failed to record hash of change-set 'root-trusted-ca'."))
	}
}

func testVaultAuth(t *testing.T, client *api.Client, options *terraform.Options) {
	var credentials = make(map[string]interface{})
	credentials["role_id"] = terraform.OutputRequired(t, options, "vault_tool_role_id")
	credentials["secret_id"] = terraform.OutputRequired(t, options, "vault_tool_secret_id")

	client.SetToken("")

	approle, err := client.Logical().Write("auth/approle/login", credentials)
	if err != nil {
		t.Fatal(errors.Wrap(err, "Failed to approle login."))
	}

	client.SetToken(approle.Auth.ClientToken)

	consul, err := client.Logical().Read("secret/vault-tool/roles/consul")
	if err != nil {
		t.Fatal(errors.Wrap(err, "Failed to read approle login for 'consul'."))
	}
	if _, ok := consul.Data["role_id"]; !ok {
		t.Fatal(errors.New("Failed to read 'role_id' under approle login for 'consul'."))
	}
	if _, ok := consul.Data["secret_id"]; !ok {
		t.Fatal(errors.New("Failed to read 'secret_id' under approle login for 'consul'."))
	}
}

func createVaultClient(address string, configureTLS func(cfg *api.Config)) (*api.Client, error) {
	config := &api.Config{Address: address}
	configureTLS(config)
	return api.NewClient(config)
}

func fetchVaultCaCertificate(t *testing.T) (*os.File) {
	insecureClient, err := createVaultClient("https://localhost:8200", func(cfg *api.Config) {
		cfg.ConfigureTLS(&api.TLSConfig{
			Insecure: true,
		})
	})
	if err != nil {
		t.Fatal(errors.Wrap(err, "Failed to create insecure vault client."))
	}

	caChain, err := insecureClient.Logical().Read("pki/cert/ca")
	if err != nil {
		t.Fatal(errors.Wrap(err, "Failed to fetch ca certificate."))
	}

	caCertificate, ok := caChain.Data["certificate"]
	if !ok {
		t.Fatal(errors.Wrap(err, "Failed to extract ca certificate."))
	}

	file, err := ioutil.TempFile("", "ca-certificate-")
	if err != nil {
		t.Fatal(errors.Wrap(err, "Failed to create temporary file for ca certificate."))
	}

	_, err = fmt.Fprintf(file, "%v", caCertificate)
	if err != nil {
		t.Fatal(errors.Wrap(err, "Failed to create temporary file for ca certificate."))
	}

	return file
}
