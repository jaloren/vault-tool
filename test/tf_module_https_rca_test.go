// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package test

import (
	"os"
	"testing"

	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/hashicorp/vault/api"
	"github.com/pkg/errors"
)

func TestHttpsRootFixture(t *testing.T) {

	terraformDirectory := "../tf/fixtures/https-rca"

	tearDownOptions := &terraform.Options{
		TerraformDir:             terraformDirectory,
		NoColor:                  true,
		RetryableTerraformErrors: map[string]string{"has active endpoints": "github.com/moby/moby/issues/17217"},
		MaxRetries:               3,
		EnvVars:                  map[string]string{"TF_WARN_OUTPUT_ERRORS": "1"},
	}

	defer terraform.Destroy(t, tearDownOptions)

	setupOptions := &terraform.Options{
		TerraformDir: terraformDirectory,
		NoColor:      true,
	}

	terraform.InitAndApply(t, setupOptions)

	// verify that the container exists, is running and healthy

	testDockerContainer(t, setupOptions)

	// fetch the ca certificate

	file := fetchVaultCaCertificate(t)

	defer os.Remove(file.Name())

	// verify that we can talk to vault and it is healthy

	client, err := createVaultClient("https://localhost:8200", func(cfg *api.Config) {
		cfg.ConfigureTLS(&api.TLSConfig{
			CACert: file.Name(),
		})
	})
	if err != nil {
		t.Fatal(errors.Wrap(err, "Failed to create vault client."))
	}

	client.SetToken(terraform.OutputRequired(t, setupOptions, "vault_root_token"))

	testVaultHealth(t, client)

	// verify we can read data from the generic secrets backend

	testVaultProvision(t, client)

	// verify we can use the vault-tool approle to create a wrapped token

	testVaultAuth(t, client, setupOptions)
}
