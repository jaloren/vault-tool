// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cli

import (
	"github.com/mitchellh/cli"
	"github.com/simon-wenmouth/vault-tool/commands"
)

func Commands(ui cli.Ui, runOpts *RunOptions, noColor bool) map[string]cli.CommandFactory {
	return map[string]cli.CommandFactory{
		"entrypoint": func() (cli.Command, error) {
			return &commands.EntrypointCommand{
				BaseCommand: &commands.BaseCommand{UI: ui},
			}, nil
		},
		"healthcheck": func() (cli.Command, error) {
			return &commands.HealthCheckCommand{
				BaseCommand: &commands.BaseCommand{UI: ui},
			}, nil
		},
		"provision": func() (cli.Command, error) {
			return &commands.ProvisionCommand{
				BaseCommand: &commands.BaseCommand{UI: ui},
				Color:       !noColor,
			}, nil
		},
		"unsafe": func() (cli.Command, error) {
			return &commands.UnsafeCommand{
				BaseCommand: &commands.BaseCommand{UI: ui},
			}, nil
		},
		"unsafe initialize": func() (cli.Command, error) {
			return &commands.UnsafeInitializeCommand{
				BaseCommand: &commands.BaseCommand{UI: ui},
			}, nil
		},
		"unsafe root-token": func() (cli.Command, error) {
			return &commands.UnsafeRootTokenCommand{
				BaseCommand: &commands.BaseCommand{UI: ui},
			}, nil
		},
		"approle": func() (cli.Command, error) {
			return &commands.ApproleCommand{
				BaseCommand: &commands.BaseCommand{UI: ui},
			}, nil
		},
		"approle server": func() (cli.Command, error) {
			return &commands.ApproleServerCommand{
				BaseCommand: &commands.BaseCommand{UI: ui},
			}, nil
		},
		"approle exec": func() (cli.Command, error) {
			return &commands.ApproleExecCommand{
				UI: ui,
			}, nil
		},
		"wait": func() (cli.Command, error) {
			return &commands.WaitCommand{
				UI: ui,
			}, nil
		},
	}
}
