// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package commands

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"

	"github.com/ghodss/yaml"
	"github.com/hashicorp/hcl2/hcl"
	"github.com/mitchellh/cli"
	"github.com/mitchellh/go-ps"
	"github.com/pkg/errors"
	"github.com/posener/complete"
	"github.com/simon-wenmouth/vault-tool/flags"
	"github.com/simon-wenmouth/vault-tool/provision"
	"github.com/spf13/afero"
	"github.com/zclconf/go-cty/cty"
	"github.com/zclconf/go-cty/cty/function"
	"go.mozilla.org/sops/decrypt"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"gopkg.in/src-d/go-git.v4/storage/memory"
)

type ProvisionCommand struct {
	*BaseCommand
	Stderr             io.Writer
	Color              bool
	flagMigrationsUrl  string
	flagSopsFileUrl    string
	flagSopsFileFormat string
	flagMigrationsEnv  string // should be []string but flags.StringSliceVar is currently broken
}

func (c *ProvisionCommand) Help() string {
	helpText := `
Usage: vault-tool provision [options]

      The provision command is used to configure a Vault instance through the
      application of commands encoded in a sequence of migration files.

` + c.Flags().Help()

	return strings.TrimSpace(helpText)
}

func (c *ProvisionCommand) Synopsis() string {
	return "Provision Vault"
}

func (c *ProvisionCommand) Flags() *flags.FlagSets {
	set := c.flagSet(FlagSetTLS | FlagSetClient)

	f := set.NewFlagSet("Command Options")

	f.StringVar(&flags.StringVar{
		Name:   "migrations-url",
		EnvVar: "TOOL_MIGRATIONS_URL",
		Target: &c.flagMigrationsUrl,
		Usage:  "URL to the vault-tool migrations.  Currently supported " +
			"schemes are 'file', 'http' and 'https'.",
	})

	f.StringVar(&flags.StringVar{
		Name:   "migrations-env",
		EnvVar: "TOOL_MIGRATIONS_ENV",
		Target: &c.flagMigrationsEnv,
		Usage:  "Comma-separated list of environment variables that should " +
			"be made available to the migrations via the HCL2 eval-context.",
	})

	f.StringVar(&flags.StringVar{
		Name:   "sops-file-url",
		EnvVar: "TOOL_SOPS_FILE_URL",
		Target: &c.flagSopsFileUrl,
		Usage:  "URL to a SOPS file.  Currently supported schemes are 'file', " +
			"'http' and 'https'.",
	})

	f.StringVar(&flags.StringVar{
		Name:    "sops-file-format",
		EnvVar:  "TOOL_SOPS_FILE_FORMAT",
		Default: "yaml",
		Target:  &c.flagSopsFileFormat,
		Usage:  "The format of the SOPS file, either 'json' or 'yaml'.",
	})

	return set
}

func (c *ProvisionCommand) AutocompleteArgs() complete.Predictor {
	return complete.PredictNothing
}

func (c *ProvisionCommand) AutocompleteFlags() complete.Flags {
	return c.Flags().Completions()
}

func (c *ProvisionCommand) Run(args []string) int {
	f := c.Flags()

	if err := f.Parse(args); err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	fs, path, err := migrations(c.flagMigrationsUrl)
	if err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	data, err := secrets(c.flagSopsFileUrl, c.flagSopsFileFormat)
	if err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	hostname, ipList, dnsList, err := networkInformation()
	if err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	var ipSans []string
	for _, ip := range ipList {
		ipSans = append(ipSans, ip.String())
	}

	var domains []cty.Value
	var domain cty.Value
	for _, dns := range dnsList {
		bits := strings.Split(dns, ".")
		if len(bits) >= 3 {
			last := strings.Join(bits[1:], ".")
			domains = append(domains, cty.StringVal(last))
			if strings.HasPrefix(dns, hostname) {
				domain = cty.StringVal(last)
			}
		}
	}

	migrationsEnvStr := os.Getenv("TOOL_MIGRATIONS_ENV")
	migrationsEnv := strings.Split(migrationsEnvStr, ",")
	c.UI.Info(fmt.Sprintf("env-variables (env): %v", migrationsEnvStr))
	c.UI.Info(fmt.Sprintf("env-variables (got): %v", c.flagMigrationsEnv))

	env := make(map[string]cty.Value)
	for _, key := range migrationsEnv {
		c.UI.Info(fmt.Sprintf("key: %s", key))
		if val, ok := os.LookupEnv(key); ok {
			c.UI.Info(fmt.Sprintf("key: %s; val: %s", key, val))
			env[key] = cty.StringVal(val)
		}
	}

	var val = cty.MapValEmpty(cty.String)
	if len(env) > 0 {
		val = cty.MapVal(env)
	}

	ctx := &hcl.EvalContext{
		Variables: map[string]cty.Value{
			"net": cty.MapVal(map[string]cty.Value{
				"common_name": cty.StringVal(hostname),
				"domain":      domain,
				"dns_sans":    cty.StringVal(strings.Join(dnsList, ",")),
				"ip_sans":     cty.StringVal(strings.Join(ipSans, ",")),
			}),
			"pki": cty.MapVal(map[string]cty.Value{
				"issuing_ca":  cty.StringVal(c.flagTlsCACertificate),
				"certificate": cty.StringVal(c.flagTlsCertificate),
				"private_key": cty.StringVal(c.flagTlsPrivateKey),
				"csr":         cty.StringVal(strings.Replace(c.flagTlsCertificate, ".cert.", ".csr.", 1)),
			}),
			"env": val,
		},
		Functions: map[string]function.Function{
			"secret": function.New(&function.Spec{
				Params: []function.Parameter{
					{
						Name:         "selector",
						Type:         cty.String,
						AllowNull:    false,
						AllowUnknown: false,
					},
				},
				Type: function.StaticReturnType(cty.String),
				Impl: extract(c.UI, data),
			}),
		},
	}

	parser := provision.NewParser(fs)

	changeLog, diagnostics := parser.LoadChangeLogDir(path, ctx)
	if len(diagnostics) > 0 {
		for _, diagnostic := range diagnostics {
			switch diagnostic.Severity {
			case hcl.DiagError:
				c.UI.Error(diagnostic.Error())
			case hcl.DiagWarning:
				c.UI.Warn(diagnostic.Error())
			}
		}
		return 1
	}

	client, err := c.client()
	if err != nil {
		c.UI.Error(errors.Wrap(err, "error creating vault client").Error())
		return 1
	}

	if err := changeLog.Run(client, c.UI); err != nil {
		c.UI.Error(err.Error())
		return 1
	} else {
		c.UI.Info("Successfully applied all change-sets.")
	}

	if strings.Contains(client.Address(), "localhost") || strings.Contains(client.Address(), "127.0.0.1") {

		c.UI.Info("Attempting to signal vault to request that it reload its configuration.")
		processes, err := ps.Processes()
		if err != nil {
			c.UI.Error(errors.Wrap(err, "Error listing processes").Error())
			return 1
		}

		var signalled = false
		for _, process := range processes {
			if process.Executable() == "vault" {

				c.UI.Info(fmt.Sprintf("Found vault with PID: %v", process.Pid()))
				vault, err := os.FindProcess(process.Pid())
				if err != nil {
					c.UI.Error(errors.Wrap(err, fmt.Sprintf("Failed to get vault process with PID: %v", process.Pid())).Error())
					return 1
				}

				err = vault.Signal(syscall.SIGHUP)
				if err != nil {
					c.UI.Error(errors.Wrap(err, fmt.Sprintf("Failed to signal vault process with PID: %v", process.Pid())).Error())
					return 1
				}

				c.UI.Info(fmt.Sprintf("Successfully signalled vault process with PID: %v", process.Pid()))
				signalled = true
				break
			}
		}

		if !signalled {
			c.UI.Warn("Failed to find vault process.")
			return 1
		}
	}

	return 0
}

func migrations(src string) (afero.Fs, string, error) {
	migrationsUrl, err := url.Parse(src)
	if err != nil {
		return nil, "", errors.Wrap(err, fmt.Sprintf("migrations-url %s is not a valid URL", src))
	}

	switch migrationsUrl.Scheme {
	case "ssh":
	case "http":
	case "https":
		return migrationsFromUrl(migrationsUrl)
	case "file":
		return migrationsFromFs(migrationsUrl.Path)
	}

	return nil, "", errors.Errorf("unsupported scheme in migrations-url '%s'", migrationsUrl.String())
}

func migrationsFromUrl(migrationsUrl *url.URL) (afero.Fs, string, error) {
	repo, path, err := splitUrl(migrationsUrl)
	if err != nil {
		return nil, "", errors.Wrap(err, fmt.Sprintf("Error splitting migrations-url: %s", migrationsUrl.String()))
	}

	_, tree, err := cloneRepository(repo)
	if err != nil {
		return nil, "", errors.Wrap(err, fmt.Sprintf("Error cloning repository at migrations-url: %s", migrationsUrl.String()))
	}

	fs := afero.NewMemMapFs()
	err = tree.Files().ForEach(copyRepositoryIntoFs(fs))
	if err != nil {
		return nil, "", errors.Wrap(err, "error copying tree into afero fs")
	}
	return fs, path, nil
}

func migrationsFromFs(path string) (afero.Fs, string, error) {
	migrationsDir, err := os.Stat(path)
	if err != nil {
		return nil, "", errors.Wrap(err, fmt.Sprintf("error getting FileInfo for migrations-url path '%s'", path))
	}
	if !migrationsDir.IsDir() {
		return nil, "", errors.Wrap(err, fmt.Sprintf("migrations-url path '%s' is not a directory", path))
	}
	return afero.NewOsFs(), path, nil
}

func secrets(src, srcFormat string) (interface{}, error) {
	if strings.TrimSpace(src) == "" {
		// client has not provided secrets ...
		return make(map[string]interface{}), nil
	}

	if !(srcFormat == "json" || srcFormat == "yaml") {
		return nil, errors.Errorf("sops-file '%s' is not a regular file", src)
	}

	secretsUrl, err := url.Parse(src)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("sops-file-url %s is not a valid URL", src))
	}

	var encryptedBytes []byte
	switch secretsUrl.Scheme {
	case "ssh":
	case "http":
	case "https":
		encryptedBytes, err = secretsFromUrl(secretsUrl)
	case "file":
		encryptedBytes, err = secretsFromFs(secretsUrl.Path)
	default:
		return nil, errors.Errorf("unsupported scheme in sops-file-url '%s'", secretsUrl.String())
	}

	decryptedBytes, err := decrypt.Data(encryptedBytes, srcFormat)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("unable to decrypt sops-file-url '%s'", src))
	}

	var jsonBytes []byte
	if srcFormat == "yaml" {
		jsonBytes, err = yaml.YAMLToJSON(decryptedBytes)
		if err != nil {
			return nil, errors.Wrap(err, fmt.Sprintf("unable to convert sops-file '%s' from YAML to JSON", src))
		}
	} else {
		jsonBytes = decryptedBytes
	}

	var data = new(interface{})
	err = json.Unmarshal(jsonBytes, data)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("unable to unmarshal sops-file '%s'", src))
	}
	return data, nil
}

func secretsFromFs(path string) ([]byte, error) {
	sopsFile, err := os.Stat(path)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("unable to inspect sops-file-url '%s'", path))
	}
	if sopsFile.IsDir() || !sopsFile.Mode().IsRegular() {
		return nil, errors.Errorf("sops-file-url '%s' is not a regular file", path)
	}

	encryptedBytes, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("error reading sops-file-url '%s'", path))
	}
	return encryptedBytes, nil
}

func secretsFromUrl(secretsUrl *url.URL) ([]byte, error) {
	repo, path, err := splitUrl(secretsUrl)
	if err != nil {
		return nil, errors.Wrap(err, "Error splitting sops-file-url")
	}
	_, tree, err := cloneRepository(repo)

	file, err := tree.File(path)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("repository '%s' does not contain the sops file: %s", repo, path))
	}

	reader, err := file.Reader()
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("error reading sops file '%s' in repository '%s'", path, repo))
	}

	encryptedBytes, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("error reading sops file '%s' in repository '%s'", path, repo))
	}
	return encryptedBytes, nil
}

func extract(ui cli.Ui, data interface{}) function.ImplFunc {
	return func(args []cty.Value, retType cty.Type) (cty.Value, error) {
		selector := args[0].AsString()
		segments := strings.Split(selector, ".")
		var path []string
		var context = data
		for _, segment := range segments {
			key := strings.TrimSpace(segment)
			if key == "" {
				if len(path) == 0 {
					// a leading '.' is fine (for people who are used to `jq`)
					continue
				} else {
					return cty.NilVal, errors.New(fmt.Sprintf("empty segment in selector: '%s'", selector))
				}
			}
			if len(segment) >= 3 && strings.HasPrefix(segment, "[") && strings.HasSuffix(segment, "]") {
				// array
				index, err := strconv.Atoi(segment[1 : len(segment)-1])
				if err != nil {
					return cty.NilVal, errors.Wrap(err, fmt.Sprintf("segment '%s' in selector '%s' is not an integer index", segment, selector))
				}
				if v, ok := context.([]interface{}); ok {
					if len(v) <= index {
						return cty.NilVal, errors.New(fmt.Sprintf("index '%d' in array at path '%s' is out of range", index, strings.Join(path, ".")))
					}
					context = v[index]
				} else {
					return cty.NilVal, errors.New(fmt.Sprintf("data at path '%s' in selector '%s' is not an array", strings.Join(path, "."), selector))
				}
			} else {
				// map
				if v, ok := context.(map[string]interface{}); ok {
					if val, ok := v[segment]; ok {
						context = val
					} else {
						return cty.NilVal, errors.New(fmt.Sprintf("key '%s' in object at path '%s' does not exist", segment, strings.Join(path, ".")))
					}
				} else {
					return cty.NilVal, errors.New(fmt.Sprintf("data at path '%s' in selector '%s' is not an object", strings.Join(path, "."), selector))
				}
			}
			path = append(path, segment)
		}
		ui.Info(fmt.Sprintf("selector: %v; result=%v", selector, context))
		switch v := context.(type) {
		case bool:
			return cty.StringVal(strconv.FormatBool(v)), nil
		case int:
			return cty.StringVal(strconv.Itoa(v)), nil
		case string:
			return cty.StringVal(v), nil
		default:
			return cty.NilVal, errors.New(fmt.Sprintf("selector '%s' did not result in a supported primitive", selector))
		}
	}
}

func cloneRepository(src string) (*git.Repository, *object.Tree, error) {
	repository, err := git.Clone(memory.NewStorage(), nil, &git.CloneOptions{URL: src})
	if err != nil {
		return nil, nil, errors.Wrap(err, fmt.Sprintf("error cloning repository: %s", src))
	}
	ref, err := repository.Head()
	if err != nil {
		return nil, nil, errors.Wrap(err, fmt.Sprintf("error getting HEAD ref in repository: %s", src))
	}
	commit, err := repository.CommitObject(ref.Hash())
	if err != nil {
		return nil, nil, errors.Wrap(err, fmt.Sprintf("error getting HEAD commit in repository: %s", src))
	}
	tree, err := commit.Tree()
	if err != nil {
		return nil, nil, errors.Wrap(err, fmt.Sprintf("error getting HEAD tree in repository: %s", src))
	}
	if tree == nil {
		return nil, nil, errors.Wrap(err, fmt.Sprintf("error getting HEAD tree in repository: %s (nil)", src))
	}
	return repository, tree, nil
}

func copyRepositoryIntoFs(fs afero.Fs) func(f *object.File) error {
	return func(f *object.File) error {
		dir := filepath.Dir(f.Name)
		err := fs.MkdirAll(dir, 0755)
		if err != nil {
			return errors.Wrap(err, fmt.Sprintf("error creating directory '%s'", dir))
		}
		mode, err := f.Mode.ToOSFileMode()
		if err != nil {
			return errors.Wrap(err, fmt.Sprintf("error getting file mode of '%s': %s", f.Name, f.Mode.String()))
		}
		file, err := fs.OpenFile(f.Name, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, mode)
		if err != nil {
			return errors.Wrap(err, fmt.Sprintf("error opening file '%s' for writing", f.Name))
		}
		readCloser, err := f.Reader()
		if err != nil {
			return errors.Wrap(err, fmt.Sprintf("error opening file '%s' for reading", f.Name))
		}
		_, err = io.Copy(file, readCloser)
		if err != nil {
			return errors.Wrap(err, fmt.Sprintf("error copying file '%s'", f.Name))
		}
		return nil
	}
}

func splitUrl(src *url.URL) (string, string, error) {
	index := strings.Index(src.Path, "//")
	if index == -1 {
		return "", "", errors.Errorf("url '%v' must indicate end of repository path with '//'", src)
	}

	idx := strings.Index(src.Path, "//")
	del := len(src.Path) - idx

	repo := src.String()
	repo = repo[0 : len(repo)-del]

	path := src.String()
	path = path[len(path)-del+2:]

	return repo, path, nil
}
