// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package commands

import (
	"strings"

	"github.com/posener/complete"
	"github.com/simon-wenmouth/vault-tool/flags"
)

type UnsafeRootTokenCommand struct {
	*BaseCommand
}

func (c *UnsafeRootTokenCommand) Help() string {
	helpText := `
Usage: vault-tool unsafe root-token [options]

      The unsafe root-token command will print the root token to standard-out.

      This command uses the initialization data stored on the file system in
      plain text at the location "~/.vault-unsafe-initialization".

      If you can run this outside of a local development environment to get a
      vault root token you have done something horribly wrong. Do not be that
      person.

` + c.Flags().Help()

	return strings.TrimSpace(helpText)
}

func (c *UnsafeRootTokenCommand) Synopsis() string {
	return "(Unsafe) Vault Root Token"
}

func (c *UnsafeRootTokenCommand) Flags() *flags.FlagSets {
	return c.flagSet(FlagSetNone)
}

func (c *UnsafeRootTokenCommand) AutocompleteArgs() complete.Predictor {
	return complete.PredictNothing
}

func (c *UnsafeRootTokenCommand) AutocompleteFlags() complete.Flags {
	return c.Flags().Completions()
}

func (c *UnsafeRootTokenCommand) Run(args []string) int {
	f := c.Flags()

	if err := f.Parse(args); err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	initializationData, err := unsafeInitializationData()
	if err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	c.UI.Info(initializationData.RootToken)
	return 0
}
