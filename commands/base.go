// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package commands

import (
	"sync"

	"github.com/hashicorp/vault/api"
	"github.com/mitchellh/cli"
	"github.com/pkg/errors"
	"github.com/simon-wenmouth/vault-tool/flags"
	"net"
	"os"
	"strings"
	"github.com/posener/complete"
)

type BaseCommand struct {
	UI cli.Ui

	flags     *flags.FlagSets
	flagsOnce sync.Once

	flagTlsDisableClient bool
	flagTlsCertificate   string
	flagTlsPrivateKey    string
	flagTlsCACertificate string
	flagTlsProvision     string

	flagVaultAddr         string
	flagVaultToken        string
	flagVaultWrappedToken string
}

type FlagSetBit uint

const (
	FlagSetNone FlagSetBit = 1 << iota
	FlagSetClient
	FlagSetTLS
)

func (c *BaseCommand) flagSet(bit FlagSetBit) *flags.FlagSets {
	c.flagsOnce.Do(func() {
		set := flags.NewFlagSets(c.UI)

		if bit&FlagSetTLS != 0 {
			f := set.NewFlagSet("TLS Options")

			tlsPath := "/opt/vault/config/keys/"

			f.BoolVar(&flags.BoolVar{
				Name:    "tls-disable-client",
				Default: false,
				EnvVar:  api.EnvVaultInsecure,
				Target:  &c.flagTlsDisableClient,
				Usage:  "Disable verification of TLS certificates. Using this option " +
					"is highly discouraged and decreases the security of data transmissions " +
					"to and from the Vault server.",
			})
			f.StringVar(&flags.StringVar{
				Name:       "tls-client-cert",
				Default:    tlsPath + "server.cert.pem",
				EnvVar:     api.EnvVaultClientCert,
				Target:     &c.flagTlsCertificate,
				Completion: complete.PredictFiles("*"),
				Usage:      "Path on the local disk to a single PEM-encoded CA " +
					"certificate to use for TLS authentication to the Vault server. " +
					"If this flag is specified, -tls-client-key is also required.",
			})
			f.StringVar(&flags.StringVar{
				Name:       "tls-client-key",
				Default:    tlsPath + "server.key.pem",
				EnvVar:     api.EnvVaultClientKey,
				Target:     &c.flagTlsPrivateKey,
				Completion: complete.PredictFiles("*"),
				Usage:      "Path on the local disk to a single PEM-encoded private " +
					"key matching the client certificate from -tls-client-cert.",
			})
			f.StringVar(&flags.StringVar{
				Name:       "tls-ca-certificate",
				Default:    tlsPath + "server.ca.pem",
				EnvVar:     api.EnvVaultCACert,
				Target:     &c.flagTlsCACertificate,
				Completion: complete.PredictFiles("*"),
				Usage:      "Path on the local disk to a single PEM-encoded CA " +
					"certificate to verify the Vault server's SSL certificate.",
			})
		}

		if bit&FlagSetClient != 0 {
			f := set.NewFlagSet("Vault Options")

			f.StringVar(&flags.StringVar{
				Name:   "vault-addr",
				EnvVar: api.EnvVaultAddress,
				Target: &c.flagVaultAddr,
				Usage:  "Address of the Vault server.",
			})
			f.StringVar(&flags.StringVar{
				Name:   "vault-token",
				EnvVar: api.EnvVaultToken,
				Target: &c.flagVaultToken,
				Usage:  "A vault token.",
			})
			f.StringVar(&flags.StringVar{
				Name:   "vault-wrapped-token",
				EnvVar: "VAULT_WRAPPED_TOKEN",
				Target: &c.flagVaultWrappedToken,
				Usage:  "A vault wrapped token.",
			})
		}

		c.flags = set
	})

	return c.flags
}

func (c *BaseCommand) client() (*api.Client, error) {
	config := api.DefaultConfig()
	if c.flagVaultAddr != "" {
		config.Address = c.flagVaultAddr
	}
	if c.flagTlsDisableClient {
		config.ConfigureTLS(&api.TLSConfig{Insecure: true})
	} else {
		tlsConfig := &api.TLSConfig{}
		useCustomTLS := false
		if c.flagTlsCACertificate != "" {
			if _, err := os.Stat(c.flagTlsCACertificate); err == nil {
				tlsConfig.CACert = c.flagTlsCACertificate
				useCustomTLS = true
			}
		}
		if c.flagTlsCertificate != "" {
			if _, err := os.Stat(c.flagTlsCertificate); err == nil {
				tlsConfig.ClientCert = c.flagTlsCertificate
				useCustomTLS = true
			}
		}
		if c.flagTlsPrivateKey != "" {
			if _, err := os.Stat(c.flagTlsPrivateKey); err == nil {
				tlsConfig.ClientKey = c.flagTlsPrivateKey
				useCustomTLS = true
			}
		}
		if useCustomTLS {
			config.ConfigureTLS(tlsConfig)
		}
	}

	client, err := api.NewClient(config)
	if err != nil {
		return nil, errors.Wrap(err, "error creating vault client")
	}

	if c.flagVaultWrappedToken != "" {
		secret, err := client.Logical().Unwrap(c.flagVaultWrappedToken)
		if err != nil {
			return nil, errors.Wrap(err, "error unwrapping token")
		}

		c.flagVaultToken = secret.Data["Token"].(string)
		client.SetToken(c.flagVaultToken)
	} else {
		if c.flagVaultToken != "" {
			client.SetToken(c.flagVaultToken)
		}
	}

	return client, nil
}

func networkInformation() (string, []net.IP, []string, error) {
	hostname, err := os.Hostname()
	if err != nil {
		return "", nil, nil, errors.Wrap(err, "failed to get hostname")
	} else {
		hostname = strings.Split(hostname, ".")[0]
	}

	ifaceArr, err := net.Interfaces()
	if err != nil {
		return "", nil, nil, errors.Wrap(err, "failed to get interface list")
	}

	var ipArr []net.IP
	var dnsArr []string
	for _, iface := range ifaceArr {
		addrArr, err := iface.Addrs()
		if err != nil {
			return "", nil, nil, errors.Wrap(err, "failed to get addresses for interface")
		}
		for _, addr := range addrArr {
			if ip, ok := addr.(*net.IPNet); ok {
				if ipv4 := ip.IP.To4(); ipv4 != nil {
					ipArr = append(ipArr, ipv4)
					if ipText, err := ipv4.MarshalText(); err == nil {
						if hosts, err := net.LookupAddr(string(ipText)); err == nil {
							for _, host := range hosts {
								if strings.HasSuffix(host, ".") {
									dnsArr = append(dnsArr, host[:len(host)-1])
								} else {
									dnsArr = append(dnsArr, host)
								}
							}
						}
					}
				}
			}
		}
	}

	return hostname, ipArr, dnsArr, nil
}

func lookupInterfaceIP(interfaceName string) (*net.IP, error) {
	iface, err := net.InterfaceByName(interfaceName)
	if err != nil {
		return nil, errors.Wrap(err, "no interface with supplied name")
	}

	addrs, err := iface.Addrs()
	if err != nil {
		return nil, errors.Wrap(err, "error getting addrs from interface")
	}

	for _, addr := range addrs {
		if ip, ok := addr.(*net.IPNet); ok {
			if ipv4 := ip.IP.To4(); ipv4 != nil {
				return &ipv4, nil
			}
		}
	}

	return nil, errors.New("no ip-address for interface")
}
