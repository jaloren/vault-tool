// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package commands

import (
	"github.com/mitchellh/cli"
	"github.com/posener/complete"
	"github.com/simon-wenmouth/vault-tool/flags"
	"strings"
)

type UnsafeCommand struct {
	*BaseCommand
}

func (c *UnsafeCommand) Help() string {
	helpText := `
Usage: vault-tool unsafe {command} [options]

      The unsafe commands exist to make developers lives simpler.

      If you are an operator, please do not use these commands.

`
	return strings.TrimSpace(helpText)
}

func (c *UnsafeCommand) Synopsis() string {
	return "Unsafe Commands"
}

func (c *UnsafeCommand) Flags() *flags.FlagSets {
	return c.flagSet(FlagSetNone)
}

func (c *UnsafeCommand) AutocompleteArgs() complete.Predictor {
	return complete.PredictNothing
}

func (c *UnsafeCommand) AutocompleteFlags() complete.Flags {
	return c.Flags().Completions()
}

func (c *UnsafeCommand) Run(args []string) int {
	return cli.RunResultHelp
}
