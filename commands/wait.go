// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package commands

import (
	"fmt"
	"net"
	"os"
	"strings"
	"syscall"
	"time"

	"github.com/mitchellh/cli"
	"github.com/pkg/errors"
	"github.com/posener/complete"
	"github.com/simon-wenmouth/vault-tool/flags"
)

type WaitCommand struct {
	UI           cli.Ui
	flagPort     int
	flagPoll     time.Duration
	flagTimeout  time.Duration
	flagDuration time.Duration
	flagVerbose  bool
}

func (c *WaitCommand) Help() string {
	helpText := `
Usage: vault-tool wait [options]

      The wait command is used by scripts to block until the Vault instance
      has started accepting TCP connections.

` + c.Flags().Help()

	return strings.TrimSpace(helpText)
}

func (c *WaitCommand) Synopsis() string {
	return "Wait for Vault"
}

func (c *WaitCommand) Flags() *flags.FlagSets {
	set := flags.NewFlagSets(c.UI)

	f := set.NewFlagSet("Command Options")

	f.IntVar(&flags.IntVar{
		Name:    "port",
		Default: 8200,
		Target:  &c.flagPort,
		Usage:  "Specifies the port number to bind to for connection.",
	})

	f.DurationVar(&flags.DurationVar{
		Name:    "timeout",
		Default: 500 * time.Millisecond,
		Target:  &c.flagTimeout,
		Usage:   "Specifies the TCP connect timeout.",
	})

	f.DurationVar(&flags.DurationVar{
		Name:    "duration",
		Default: 5 * time.Second,
		Target:  &c.flagDuration,
		Usage:   "Specifies the maximum amount of time to wait for the port " +
			"to start accepting connections.",
	})

	f.DurationVar(&flags.DurationVar{
		Name:    "poll",
		Default: 1 * time.Second,
		Target:  &c.flagPoll,
		Usage:   "Specifies the polling interval between connection attempts.",
	})

	f.BoolVar(&flags.BoolVar{
		Name:    "verbose",
		Default: false,
		Target:  &c.flagVerbose,
		Usage:   "Specifies whether to print failed connection attempts.",
	})

	return set
}

func (c *WaitCommand) AutocompleteArgs() complete.Predictor {
	return complete.PredictNothing
}

func (c *WaitCommand) AutocompleteFlags() complete.Flags {
	return c.Flags().Completions()
}

func (c *WaitCommand) Run(args []string) int {
	f := c.Flags()

	if err := f.Parse(args); err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	if c.flagPort <= 0 || c.flagPort >= 65535 {
		c.UI.Error(fmt.Sprintf("Argument port number is out of bounds: %d", c.flagPort))
		return 1
	}

	timeout := time.NewTimer(c.flagDuration)
	defer timeout.Stop()

	ticker := time.NewTicker(c.flagPoll)
	defer ticker.Stop()

	for {
		select {
		case <-timeout.C:
			c.UI.Error("Timeout expired.")
			return 1
		case <-ticker.C:
			conn, err := net.DialTimeout("tcp4", fmt.Sprintf(":%d", c.flagPort), c.flagTimeout)
			if err != nil {
				if ne, ok := err.(*net.OpError); ok {
					if se, ok := ne.Err.(*os.SyscallError); ok {
						if se.Err == syscall.ECONNREFUSED {
							if c.flagVerbose {
								c.UI.Info("Port is not (yet) accepting connections.")
							}
							continue
						}
					}
				}
				c.UI.Error(errors.Wrap(err, "Port is not accepting connections.").Error())
				return 1
			} else {
				c.UI.Info("Port is accepting connections.")
				conn.Close()
				return 0
			}
		}
	}
}
