// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package commands

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"sync"

	"github.com/hashicorp/vault/api"
	"github.com/pkg/errors"
	"github.com/posener/complete"
	"github.com/simon-wenmouth/vault-tool/flags"
)

type ApproleServerCommand struct {
	*BaseCommand
	flagAdvertiseInterface string
	flagAdvertisePort      int
}

func (c *ApproleServerCommand) Help() string {
	helpText := `
Usage: vault-tool approle server [options]

      The approle server command starts a HTTP server that issues vault
      wrapped tokens created against an approle name.

      Be nice.  Only bind the server to the loopback device - it is not
      secure if you bind it to another interface making it available to
      other systems on the network.

` + c.Flags().Help()

	return strings.TrimSpace(helpText)
}

func (c *ApproleServerCommand) Synopsis() string {
	return "Approle Server"
}

func (c *ApproleServerCommand) Flags() *flags.FlagSets {
	set := c.flagSet(FlagSetTLS | FlagSetClient)

	f := set.NewFlagSet("Command Options")

	f.StringVar(&flags.StringVar{
		Name:    "advertise-interface",
		Default: "lo",
		EnvVar:  "TOOL_ADVERTISE_INTERFACE",
		Target:  &c.flagAdvertiseInterface,
		Usage:   "Specifies the network interface to bind to for listening.",
	})

	f.IntVar(&flags.IntVar{
		Name:    "advertise-port",
		Default: 8250,
		EnvVar:  "TOOL_ADVERTISE_PORT",
		Target:  &c.flagAdvertisePort,
		Usage:   "Specifies the port number to bind to for listening.",
	})

	return set
}

func (c *ApproleServerCommand) AutocompleteArgs() complete.Predictor {
	return complete.PredictNothing
}

func (c *ApproleServerCommand) AutocompleteFlags() complete.Flags {
	return c.Flags().Completions()
}

func (c *ApproleServerCommand) Run(args []string) int {
	f := c.Flags()

	if err := f.Parse(args); err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	ip, err := lookupInterfaceIP(c.flagAdvertiseInterface)
	if err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	port := c.flagAdvertisePort
	if port < 1 || port > 65535 {
		c.UI.Error(fmt.Sprintf("advertise-port is out of bounds: %d", port))
		return 1
	}

	if strings.Contains(c.flagVaultAddr, "localhost") || strings.Contains(c.flagVaultAddr, "127.0.0.1") {
		// telling others that *you* talk to localhost is just mean
		// so let's take our advertised ip address instead
		// worst case, we're replacing localhost with, well, localhost
		// otherwise, this is super useful. really useful.
		if addr, err := url.Parse(c.flagVaultAddr); err == nil {
			public := &url.URL{
				Scheme: addr.Scheme,
				Host:   fmt.Sprintf("%s:%s", ip.String(), addr.Port()),
			}
			c.flagVaultAddr = public.String()
		}
	} else if c.flagVaultAddr == "" {
		scheme := "https"
		if c.flagTlsDisableClient {
			scheme = "http"
		}
		c.flagVaultAddr = fmt.Sprintf("%s://%s:%s", scheme, ip.String(), "8200")
		c.UI.Info(fmt.Sprintf("Public vault server address: %s", c.flagVaultAddr))
	}

	client, err := c.client()
	if err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	caChain, err := client.Logical().Read("pki/cert/ca")
	if err != nil {
		c.UI.Error(errors.Wrap(err, "error requesting ca chain").Error())
		return 1
	}

	caCertificate, ok := caChain.Data["certificate"]
	if !ok {
		c.UI.Error(errors.Wrap(err, "response did not contain ca chain").Error())
		return 1
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/approle", c.execHandler(client, fmt.Sprintf("%v", caCertificate)))

	srv := &http.Server{
		Addr:    fmt.Sprintf("%s:%d", ip.String(), port),
		Handler: mux,
	}

	if err := srv.ListenAndServe(); err != nil {
		c.UI.Error(fmt.Sprintf("HTTP server exited with an error: %s", err))
		return 1
	}

	return 0
}

func wrappingLookupFunc(wrap bool) func(operation, path string) string {
	return func(operation, path string) string {
		if (operation == "PUT" || operation == "POST") && path == "auth/approle/login" && wrap {
			return api.DefaultWrappingTTL
		}
		return api.DefaultWrappingLookupFunc(operation, path)
	}
}

type ApproleRequest struct {
	Approle string `json:"approle"`
	Wrap    bool   `json:"wrap"`
}

type ApproleResponse struct {
	Auth        *api.SecretAuth     `json:"auth"`
	WrapInfo    *api.SecretWrapInfo `json:"wrap_info"`
	VaultAddr   string              `json:"vault_addr"`
	VaultCaCert string              `json:"vault_ca_cert"`
}

func (c *ApproleServerCommand) execHandler(client *api.Client, caCertificate string) func(http.ResponseWriter, *http.Request) {
	mutex := sync.Mutex{}
	return func(rw http.ResponseWriter, req *http.Request) {
		// limit to one concurrent process
		mutex.Lock()
		defer mutex.Unlock()

		// demanding a http request body
		if req.Body == nil {
			http.Error(rw, "Please send a request body.", 400)
			return
		}

		// that looks like an approle request
		var body ApproleRequest
		err := json.NewDecoder(req.Body).Decode(&body)
		if err != nil {
			http.Error(rw, err.Error(), 400)
			return
		}

		// request the approle list
		secret, err := client.Logical().List("secret/vault-tool/roles")
		if err != nil {
			http.Error(rw, errors.Wrap(err, "Error getting approle list").Error(), 500)
			return
		}

		keys, ok := secret.Data["keys"]
		if !ok {
			http.Error(rw, "Error getting approle list: no keys", 500)
			return
		}

		vals, ok := keys.([]interface{})
		if !ok {
			http.Error(rw, "Error getting approle list: keys not []interface{}", 500)
			return
		}

		for _, val := range vals {
			if val != body.Approle {
				continue
			}

			// getting the role_id and secret_id
			credentials, err := client.Logical().Read(fmt.Sprintf("secret/vault-tool/roles/%s", body.Approle))
			if err != nil {
				http.Error(rw, errors.Wrap(err, "Error getting credentials").Error(), 500)
				return
			}

			// wrap if we were asked nicely
			client.SetWrappingLookupFunc(wrappingLookupFunc(body.Wrap))

			// authenticating
			token, err := client.Logical().Write("auth/approle/login", credentials.Data)
			if err != nil {
				http.Error(rw, errors.Wrap(err, "Error authenticating with approle credentials").Error(), 500)
				return
			}

			// making the response
			content := &ApproleResponse{
				Auth:        token.Auth,
				WrapInfo:    token.WrapInfo,
				VaultAddr:   client.Address(),
				VaultCaCert: caCertificate,
			}

			// into bytes
			bytes, err := json.Marshal(content)
			if err != nil {
				http.Error(rw, errors.Wrap(err, "Error encoding response.").Error(), 500)
				return
			}

			// that are sent to the client
			rw.Header().Add("Content-Type", "application/json")
			rw.WriteHeader(http.StatusOK)
			rw.Write(bytes)
			return
		}

		http.Error(rw, fmt.Sprintf("Not Found: %s", body.Approle), 404)
	}
}
