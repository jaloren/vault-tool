// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package commands

import (
	"bufio"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"math/big"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	"github.com/hashicorp/vault/api"
	"github.com/pkg/errors"
	"github.com/posener/complete"
	"github.com/simon-wenmouth/vault-tool/flags"
)

type EntrypointCommand struct {
	*BaseCommand

	// the path to the configuration files
	flagConfigurationPath string

	// tls certificate provisioning mode
	flagTlsProvision     string
	flagTlsDisableServer bool

	// if we request a certificate from a vault trusted root ca
	flagTrustedRootCaAddr  string
	flagTrustedRootCaToken string
	flagTrustedRootCaRole  string

	// 00-base.json
	flagClusterName     string
	flagCacheSize       int
	flagDisableCache    bool
	flagDisableMLock    bool
	flagDefaultLeaseTtl time.Duration
	flagMaxLeaseTtl     time.Duration

	// 01-listener.json
	flagAdvertiseInterface string

	// 02-storage.json
	flagStorageBackend            string
	flagFileStoragePath           string
	flagConsulAddr                string
	flagConsulCheckTimeout        time.Duration
	flagConsulConsistencyMode     string
	flagConsulDisableRegistration bool
	flagConsulMaxParallel         int
	flagConsulScheme              string
	flagConsulService             string
	flagConsulToken               string
}

func (c *EntrypointCommand) Help() string {
	helpText := `
Usage: vault-tool entrypoint [options]

      The entrypoint command is used to configure a Vault instance using
      properties supplied at the command line or via environment variables.

` + c.Flags().Help()

	return strings.TrimSpace(helpText)
}

func (c *EntrypointCommand) Synopsis() string {
	return "Docker ENTRYPOINT"
}

func (c *EntrypointCommand) Flags() *flags.FlagSets {
	set := c.flagSet(FlagSetTLS)

	f := set.NewFlagSet("Command Options")

	f.StringVar(&flags.StringVar{
		Name:    "configuration-path",
		Default: "/opt/vault/config",
		EnvVar:  "TOOL_CONFIGURATION_PATH",
		Target:  &c.flagConfigurationPath,
		Usage:   "Path on the local disk to the directory into which the" +
			"configuration files will be written.",
	})

	// tls
	f.StringVar(&flags.StringVar{
		Name:    "tls-provision",
		Default: "self-signed",
		EnvVar:  "TOOL_TLS_PROVISION",
		Target:  &c.flagTlsProvision,
		Usage:   "The manner in which the Vault TLS certificates will be " +
			"provisioned.  Valid values include: 'self-signed', 'vault' and " +
			"'no'. The 'self-signed' method uses the golang package " +
			"'crypto/x509' to create a trusted root certificate and client " +
			"certificate. This is useful create temporary certificates prior " +
			"to vault having the ability to create its own CA certificate.  " +
			"The 'vault' method makes a request to a vault instance operating " +
			"as a trusted root CA for a client certificate.  The 'no' method " +
			"generates no certificate and the server is either run insecurely " +
			"(i.e. over http) or with externally provided certificates.",
	})
	f.BoolVar(&flags.BoolVar{
		Name:    "tls-disable-server",
		Default: false,
		EnvVar:  "TOOL_TLS_DISABLE_SERVER",
		Target:  &c.flagTlsDisableServer,
		Usage:   "Disable use of TLS certificates when running the vault server.",
	})

	// trusted root ca
	f.StringVar(&flags.StringVar{
		Name:    "root-ca-addr",
		Default: "",
		EnvVar:  "TOOL_ROOT_CA_VAULT_ADDR",
		Target:  &c.flagTrustedRootCaAddr,
		Usage:   "Address of the trusted root CA vault server.",
	})
	f.StringVar(&flags.StringVar{
		Name:    "root-ca-token",
		Default: "",
		EnvVar:  "TOOL_ROOT_CA_VAULT_TOKEN",
		Target:  &c.flagTrustedRootCaToken,
		Usage:   "A wrapped vault token issued by the trusted root CA vault server.",
	})
	f.StringVar(&flags.StringVar{
		Name:    "root-ca-pki-role",
		Default: "tlscert",
		EnvVar:  "TOOL_ROOT_CA_VAULT_PKI_ROLE",
		Target:  &c.flagTrustedRootCaRole,
		Usage:   "The name of the trusted root CA vault pki role.",
	})

	// 00-base.json
	f.StringVar(&flags.StringVar{
		Name:    "cluster-name",
		Default: "vault",
		EnvVar:  "TOOL_CLUSTER_NAME",
		Target:  &c.flagClusterName,
		Usage:   "Specifies the identifier for the Vault cluster. If omitted, Vault will generate a value.",
	})
	f.IntVar(&flags.IntVar{
		Name:    "cache-size",
		Default: 32000,
		EnvVar:  "TOOL_CACHE_SIZE",
		Target:  &c.flagCacheSize,
		Usage:   "Specifies the size of the read cache used by the physical storage subsystem.",
	})
	f.BoolVar(&flags.BoolVar{
		Name:    "disable-cache",
		Default: false,
		EnvVar:  "TOOL_DISABLE_CACHE",
		Target:  &c.flagDisableCache,
		Usage:   "Disables all caches within Vault, including the read cache used by the physical storage subsystem.",
	})
	f.BoolVar(&flags.BoolVar{
		Name:    "disable-mlock",
		Default: false,
		EnvVar:  "TOOL_DISABLE_MLOCK",
		Target:  &c.flagDisableMLock,
		Usage:   "Disables the server from executing the mlock syscall. mlock prevents memory from being swapped to disk.",
	})
	f.DurationVar(&flags.DurationVar{
		Name:    "default-lease-ttl",
		Default: 768 * time.Hour,
		EnvVar:  "TOOL_DEFAULT_LEASE_TTL",
		Target:  &c.flagDefaultLeaseTtl,
		Usage:   "Specifies the default lease duration for tokens and secrets.",
	})
	f.DurationVar(&flags.DurationVar{
		Name:    "max-lease-ttl",
		Default: 768 * time.Hour,
		EnvVar:  "TOOL_MAX_LEASE_TTL",
		Target:  &c.flagMaxLeaseTtl,
		Usage:   "Specifies the maximum possible lease duration for tokens and secrets.",
	})

	// 01-listener.json
	f.StringVar(&flags.StringVar{
		Name:    "advertise-interface",
		Default: "eth0",
		EnvVar:  "TOOL_ADVERTISE_INTERFACE",
		Target:  &c.flagAdvertiseInterface,
		Usage:   "Specifies the network interface to bind to for listening.",
	})

	// 02-storage.json
	f.StringVar(&flags.StringVar{
		Name:    "storage-backend",
		Default: "file",
		EnvVar:  "TOOL_STORAGE_BACKEND",
		Target:  &c.flagStorageBackend,
		Usage:   "The backend for the durable storage of Vault's information. " +
			"Supported values include 'file' and 'consul'.",
	})
	f.StringVar(&flags.StringVar{
		Name:    "file-storage-path",
		Default: "/opt/vault/data",
		EnvVar:  "TOOL_FILE_STORAGE_PATH",
		Target:  &c.flagFileStoragePath,
		Usage:   "The absolute path on disk to the directory where the data will be stored.",
	})
	f.StringVar(&flags.StringVar{
		Name:    "consul-addr",
		Default: "localhost:8500",
		EnvVar:  "TOOL_CONSUL_ADDR",
		Target:  &c.flagConsulAddr,
		Usage:   "Specifies the address of the Consul agent to communicate with.",
	})
	f.DurationVar(&flags.DurationVar{
		Name:    "consul-check-timeout",
		Default: 5 * time.Second,
		EnvVar:  "TOOL_CONSUL_CHECK_TIMEOUT",
		Target:  &c.flagConsulCheckTimeout,
		Usage:   "Specifies the check interval used to send health check information back to Consul. ",
	})
	f.StringVar(&flags.StringVar{
		Name:    "consul-consistency-mode",
		Default: "default",
		EnvVar:  "TOOL_CONSUL_CONSISTENCY_MODE",
		Target:  &c.flagConsulConsistencyMode,
		Usage:   "Specifies the Consul consistency mode.",
	})
	f.BoolVar(&flags.BoolVar{
		Name:    "consul-disable-registration",
		Default: false,
		EnvVar:  "TOOL_CONSUL_DISABLE_REGISTRATION",
		Target:  &c.flagConsulDisableRegistration,
		Usage:   "Specifies whether Vault should register itself with Consul.",
	})
	f.IntVar(&flags.IntVar{
		Name:    "consul-max-parallel",
		Default: 128,
		EnvVar:  "TOOL_CONSUL_MAX_PARALLEL",
		Target:  &c.flagConsulMaxParallel,
		Usage:   "Specifies the maximum number of concurrent requests to Consul.",
	})
	f.StringVar(&flags.StringVar{
		Name:    "consul-scheme",
		Default: "http",
		EnvVar:  "TOOL_CONSUL_SCHEME",
		Target:  &c.flagConsulScheme,
		Usage:   "Specifies the scheme to use when communicating with Consul. " +
			"This can be set to 'http' or 'https'.",
	})
	f.StringVar(&flags.StringVar{
		Name:    "consul-service",
		Default: "vault",
		EnvVar:  "TOOL_CONSUL_SERVICE",
		Target:  &c.flagConsulScheme,
		Usage:   "Specifies the name of the service to register in Consul.",
	})
	f.StringVar(&flags.StringVar{
		Name:    "consul-token",
		Default: "vault",
		EnvVar:  "TOOL_CONSUL_TOKEN",
		Target:  &c.flagConsulToken,
		Usage:   "Specifies the Consul ACL token with permission to read and " +
			"write from the path in Consul's key-value store. This is not a " +
			"Vault token.",
	})

	return set
}

func (c *EntrypointCommand) AutocompleteArgs() complete.Predictor {
	return complete.PredictNothing
}

func (c *EntrypointCommand) AutocompleteFlags() complete.Flags {
	return c.Flags().Completions()
}

func (c *EntrypointCommand) Run(args []string) int {
	f := c.Flags()

	if err := f.Parse(args); err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	directories := []string{
		c.flagConfigurationPath,
		filepath.Dir(c.flagTlsCACertificate),
		filepath.Dir(c.flagTlsCertificate),
		filepath.Dir(c.flagTlsPrivateKey),
	}

	if err := createDirectories(directories...); err != nil {
		c.UI.Error(errors.Wrap(err, "could not create directories").Error())
		return 1
	}

	switch c.flagTlsProvision {
	case "self-signed":
		if err := c.provisionCertificateUsingGolang(); err != nil {
			c.UI.Error(errors.Wrap(err, "error creating self-signed certificates").Error())
			return 1
		}
	case "vault":
		if err := c.provisionCertificateUsingVault(); err != nil {
			c.UI.Error(errors.Wrap(err, "error requesting certificate from vault").Error())
			return 1
		}
	case "no":
		break
	default:
		c.UI.Error(errors.New("unknown certificate provisioning option").Error())
		return 1
	}

	ip, err := lookupInterfaceIP(c.flagAdvertiseInterface)
	if err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	// 00-base.json
	baseData := make(map[string]interface{})
	baseData["cluster_name"] = c.flagClusterName
	baseData["cache_size"] = c.flagCacheSize
	baseData["disable_cache"] = c.flagDisableCache
	baseData["disable_mlock"] = c.flagDisableMLock
	baseData["default_lease_ttl"] = humanDuration(c.flagDefaultLeaseTtl)
	baseData["max_lease_ttl"] = humanDuration(c.flagMaxLeaseTtl)

	baseFile, err := os.Create(filepath.Join(c.flagConfigurationPath, "00-base.json"))
	defer baseFile.Close()
	baseJson, err := json.Marshal(baseData)
	baseFile.Write(baseJson)

	// 01-listener.json
	tcpData := make(map[string]interface{})
	tcpData["address"] = "0.0.0.0:8200"
	tcpData["cluster_address"] = ip.String() + ":8201"
	if c.flagTlsDisableServer {
		tcpData["tls_disable"] = "true"
	} else {
		tcpData["tls_disable"] = "false"
		tcpData["tls_cert_file"] = c.flagTlsCertificate
		tcpData["tls_key_file"] = c.flagTlsPrivateKey
	}
	listenerData := make(map[string]interface{})
	listenerData["tcp"] = tcpData
	listenerWrap := make(map[string]interface{})
	listenerWrap["listener"] = listenerData

	listenerFile, err := os.Create(filepath.Join(c.flagConfigurationPath, "01-listener.json"))
	defer listenerFile.Close()
	listenerJson, err := json.Marshal(listenerWrap)
	listenerFile.Write(listenerJson)

	// 02-storage.json
	storageData := make(map[string]interface{})
	storageWrap := make(map[string]interface{})
	storageWrap["storage"] = storageData
	switch c.flagStorageBackend {
	case "file":
		fileData := make(map[string]interface{})
		fileData["path"] = c.flagFileStoragePath
		storageData["file"] = fileData
	case "consul":
		redirectScheme := "https"
		if c.flagTlsDisableServer {
			redirectScheme = "http"
		}
		consulData := make(map[string]interface{})
		consulData["address"] = c.flagConsulAddr
		consulData["check_timeout"] = humanDuration(c.flagConsulCheckTimeout)
		consulData["consistency_mode"] = c.flagConsulConsistencyMode
		consulData["disable_registration"] = c.flagConsulDisableRegistration
		consulData["max_parallel"] = c.flagConsulMaxParallel
		consulData["path"] = "vault"
		consulData["scheme"] = c.flagConsulScheme
		consulData["service"] = c.flagConsulService
		consulData["token"] = c.flagConsulToken
		consulData["cluster_addr"] = redirectScheme + "://" + ip.String() + ":8201"
		consulData["redirect_addr"] = redirectScheme + "://" + ip.String() + ":8200"
		if !c.flagTlsDisableServer {
			consulData["tls_ca_file"] = c.flagTlsCACertificate
			consulData["tls_cert_file"] = c.flagTlsCertificate
			consulData["tls_key_file"] = c.flagTlsPrivateKey
		}
		storageData["consul"] = consulData
	}

	storageFile, err := os.Create(filepath.Join(c.flagConfigurationPath, "02-storage.json"))
	defer storageFile.Close()
	storageJson, err := json.Marshal(storageWrap)
	storageFile.Write(storageJson)

	vault, err := exec.LookPath("vault")
	if err != nil {
		c.UI.Error(errors.Wrap(err, "vault executable not in path").Error())
		return 1
	}

	vaultArgs := []string{"vault", "server", "-config=" + c.flagConfigurationPath, "-log-level=info"}
	vaultEnv := os.Environ()

	c.UI.Info(fmt.Sprintf("vault path: %v", vault))
	c.UI.Info(fmt.Sprintf("vault args: %v", strings.Join(vaultArgs, " ")))

	err = syscall.Exec(vault, vaultArgs, vaultEnv)
	if err != nil {
		c.UI.Error(errors.Wrap(err, "failed to exec vault").Error())
		return 1
	}

	return 0 // but really not ... because the exec succeeded
}

func (c *EntrypointCommand) provisionCertificateUsingVault() error {
	c.UI.Info("Provisioning TLS certificates using Vault.")
	c.UI.Info(fmt.Sprintf("address: %s", c.flagTrustedRootCaAddr))
	c.UI.Info(fmt.Sprintf("token:   %s", c.flagTrustedRootCaToken))
	c.UI.Info(fmt.Sprintf("role:    %s", c.flagTrustedRootCaRole))
	c.UI.Info(fmt.Sprintf("ca-cert: %s", c.flagTlsCACertificate))

	hostname, ips, names, err := networkInformation()
	if err != nil {
		return errors.Wrap(err, "failed to get network information")
	}

	config := &api.Config{Address: c.flagTrustedRootCaAddr}
	if _, err := os.Stat(c.flagTlsCACertificate); err != nil {
		config.ConfigureTLS(&api.TLSConfig{Insecure: true})
	} else {
		config.ConfigureTLS(&api.TLSConfig{CACert: c.flagTlsCACertificate})
	}

	client, err := api.NewClient(config)
	if err != nil {
		return errors.Wrap(err, "error creating vault api client")
	}

	token, err := client.Logical().Unwrap(c.flagTrustedRootCaToken)
	if err != nil {
		return errors.Wrap(err, "error unwrapping token")
	}

	c.flagTrustedRootCaToken = token.Auth.ClientToken

	client.SetToken(c.flagTrustedRootCaToken)

	cn := hostname
	var altNames []string
	for i, name := range names {
		name = strings.TrimSuffix(name, ".")
		if i == 0 {
			cn = name
		}
		altNames = append(altNames, name)
	}

	var ipSans []string
	for _, ip := range ips {
		ipSans = append(ipSans, ip.String())
	}

	options := make(map[string]interface{})
	options["common_name"] = cn
	options["alt_names"] = strings.Join(altNames, ",")
	options["ttl"] = "4380h"
	options["ip_sans"] = strings.Join(ipSans, ",")
	options["format"] = "pem"

	secret, err := client.Logical().Write(fmt.Sprintf("pki/issue/%s", c.flagTrustedRootCaRole), options)
	if err != nil {
		return errors.Wrap(err, "error requesting pki")
	}

	caCertificateFile, err := os.Create(c.flagTlsCACertificate)
	if err != nil {
		return errors.Wrap(err, "failed to open ca certificate file for writing")
	}
	defer caCertificateFile.Close()

	certificateFile, err := os.Create(c.flagTlsCertificate)
	if err != nil {
		return errors.Wrap(err, "failed to open certificate file for writing")
	}
	defer certificateFile.Close()

	privateKeyFile, err := os.OpenFile(c.flagTlsPrivateKey, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		return errors.Wrap(err, "failed to open private key file for writing")
	}
	defer privateKeyFile.Close()

	var n int

	certificateWriter := bufio.NewWriter(certificateFile)
	n, err = fmt.Fprint(certificateWriter, secret.Data["certificate"])
	if n == 0 || err != nil {
		return errors.Wrap(err, "error writing certificate [certificate]")
	}
	n, err = certificateWriter.WriteString("\n")
	if n == 0 || err != nil {
		return errors.Wrap(err, "error writing certificate [newline separator]")
	}
	n, err = fmt.Fprint(certificateWriter, secret.Data["issuing_ca"])
	if n == 0 || err != nil {
		return errors.Wrap(err, "error writing certificate [issuing_ca]")
	}
	certificateWriter.Flush()

	privateKeyWriter := bufio.NewWriter(privateKeyFile)
	n, err = fmt.Fprint(privateKeyWriter, secret.Data["private_key"])
	if n == 0 || err != nil {
		return errors.Wrap(err, "error writing private_key")
	}
	privateKeyWriter.Flush()

	caCertificateWriter := bufio.NewWriter(caCertificateFile)
	n, err = fmt.Fprint(caCertificateWriter, secret.Data["issuing_ca"])
	if n == 0 || err != nil {
		return errors.Wrap(err, "error writing ca certificate")
	}
	caCertificateWriter.Flush()

	return nil
}

func (c *EntrypointCommand) provisionCertificateUsingGolang() error {
	hostname, ips, names, err := networkInformation()
	if err != nil {
		return errors.Wrap(err, "failed to get network information")
	}

	caCertificateFile, err := os.Create(c.flagTlsCACertificate)
	defer caCertificateFile.Close()

	caPrivateKeyPath := filepath.Join(filepath.Dir(c.flagTlsCACertificate), "ca.key.pem")
	caPrivateKeyFile, err := os.OpenFile(caPrivateKeyPath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	defer caPrivateKeyFile.Close()

	certificateFile, err := os.Create(c.flagTlsCertificate)
	defer certificateFile.Close()

	privateKeyFile, err := os.OpenFile(c.flagTlsPrivateKey, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	defer privateKeyFile.Close()

	caCertificate := &x509.Certificate{
		SerialNumber: big.NewInt(1653),
		Subject: pkix.Name{
			Organization: []string{"github.com/simon-wenmouth"},
			Country:      []string{"US"},
			Province:     []string{"IL"},
			Locality:     []string{"Chicago"},
			CommonName:   hostname + "-ca",
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().AddDate(10, 0, 0),
		IsCA:                  true,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:              x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		BasicConstraintsValid: true,
	}

	caPrivateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return errors.Wrap(err, "failed to generate ca private key")
	}
	if err := pem.Encode(caPrivateKeyFile, &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(caPrivateKey)}); err != nil {
		return errors.Wrap(err, "failed to write ca private key")
	}

	caPublicKeyBytes, err := x509.CreateCertificate(rand.Reader, caCertificate, caCertificate, &caPrivateKey.PublicKey, caPrivateKey)
	if err != nil {
		return errors.Wrap(err, "failed to generate ca public key")
	}
	if err := pem.Encode(caCertificateFile, &pem.Block{Type: "CERTIFICATE", Bytes: caPublicKeyBytes}); err != nil {
		return errors.Wrap(err, "failed to write ca public key")
	}

	certificate := &x509.Certificate{
		SerialNumber: big.NewInt(1658),
		Subject: pkix.Name{
			Organization: []string{"github.com/simon-wenmouth"},
			Country:      []string{"US"},
			Province:     []string{"IL"},
			Locality:     []string{"Chicago"},
			CommonName:   hostname,
		},
		NotBefore:    time.Now(),
		NotAfter:     time.Now().AddDate(3, 0, 0),
		SubjectKeyId: []byte{1, 2, 3, 4, 6},
		ExtKeyUsage:  []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:     x509.KeyUsageDigitalSignature,
		IPAddresses:  ips,
		DNSNames:     names,
	}

	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return errors.Wrap(err, "failed to generate private key")
	}
	if err := pem.Encode(privateKeyFile, &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(privateKey)}); err != nil {
		return errors.Wrap(err, "failed to write private key")
	}

	certificateBytes, err := x509.CreateCertificate(rand.Reader, certificate, caCertificate, &privateKey.PublicKey, caPrivateKey)
	if err != nil {
		return errors.Wrap(err, "failed to create certificate")
	}
	if err := pem.Encode(certificateFile, &pem.Block{Type: "CERTIFICATE", Bytes: certificateBytes}); err != nil {
		return errors.Wrap(err, "failed to write certificate")
	}

	return nil
}

func createDirectories(paths ...string) error {
	for _, path := range paths {
		dir := filepath.Dir(path)
		if _, err := os.Stat(dir); os.IsNotExist(err) {
			if err := os.MkdirAll(dir, os.ModeDir); err != nil {
				return err
			}
		}
	}
	return nil
}

func humanDuration(d time.Duration) string {
	if d == 0 {
		return "0s"
	}

	s := d.String()
	if strings.HasSuffix(s, "m0s") {
		s = s[:len(s)-2]
	}
	if idx := strings.Index(s, "h0m"); idx > 0 {
		s = s[:idx+1] + s[idx+3:]
	}
	return s
}
