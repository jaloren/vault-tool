// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package commands

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/hashicorp/vault/api"
	"github.com/pkg/errors"
	"github.com/posener/complete"
	"github.com/simon-wenmouth/vault-tool/flags"
)

type UnsafeInitializeCommand struct {
	*BaseCommand
}

func (c *UnsafeInitializeCommand) Help() string {
	helpText := `
Usage: vault-tool unsafe initialize [options]

      The unsafe initialize command will run "vault init" and "vault unseal"
      (as needed) such that the Vault instance is ready to accept commands.

      This command is unsafe as it stores the initialization data on the file
      system in plain text at the location "~/.vault-unsafe-initialization".

      Please do not be the person who uses this command to initialize a
      production Vault instance.  This command exists to make local development
      a little more straightforward.

` + c.Flags().Help()

	return strings.TrimSpace(helpText)
}

func (c *UnsafeInitializeCommand) Synopsis() string {
	return "(Unsafe) Initialize Vault"
}

func (c *UnsafeInitializeCommand) Flags() *flags.FlagSets {
	return c.flagSet(FlagSetTLS | FlagSetClient)
}

func (c *UnsafeInitializeCommand) AutocompleteArgs() complete.Predictor {
	return complete.PredictNothing
}

func (c *UnsafeInitializeCommand) AutocompleteFlags() complete.Flags {
	return c.Flags().Completions()
}

func (c *UnsafeInitializeCommand) Run(args []string) int {
	f := c.Flags()

	if err := f.Parse(args); err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	client, err := c.client()
	if err != nil {
		c.UI.Error(errors.Wrap(err, "error creating vault client").Error())
		return 1
	}

	health, err := client.Sys().Health()
	if err != nil {
		c.UI.Error(errors.Wrap(err, "error unwrapping token").Error())
		return 1
	}

	if !health.Initialized {
		if err := c.unsafeInitialize(client); err != nil {
			c.UI.Error(errors.Wrap(err, "error initializing vault").Error())
			return 1
		}
	}

	if health.Sealed {
		if err := c.unsafeUnseal(client); err != nil {
			c.UI.Error(errors.Wrap(err, "error unsealing vault").Error())
			return 1
		}
	}

	return 0
}

func (c *UnsafeInitializeCommand) unsafeInitialize(client *api.Client) error {
	response, err := client.Sys().Init(&api.InitRequest{
		SecretShares:    1,
		SecretThreshold: 1,
	})
	if err != nil {
		return errors.Wrap(err, "error initializing vault")
	}

	initializationJson, err := json.Marshal(response)
	if err != nil {
		return errors.Wrap(err, "error during serialization of vault initialization data")
	}

	initializationPath := filepath.Join(unsafeInitializationDataPath(), ".vault-unsafe-initialization")
	initializationFile, err := os.OpenFile(initializationPath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		return errors.Wrap(err, "error opening file for writing")
	}
	defer initializationFile.Close()

	initializationFile.Write(initializationJson)

	return nil
}

func (c *UnsafeInitializeCommand) unsafeUnseal(client *api.Client) error {
	data, err := unsafeInitializationData()
	if err != nil {
		return err
	}

	response, err := client.Sys().Unseal(data.Keys[0])
	if err != nil {
		return err
	}

	if response.Sealed {
		return errors.New("vault is still sealed")
	}

	return nil
}

func unsafeInitializationData() (*api.InitResponse, error) {
	initializationPath := filepath.Join(unsafeInitializationDataPath(), ".vault-unsafe-initialization")
	initializationFile, err := ioutil.ReadFile(initializationPath)
	if err != nil {
		return nil, errors.Wrap(err, "error opening vault initialization data file for reading")
	}

	var data = api.InitResponse{}
	if err := json.Unmarshal(initializationFile, &data); err != nil {
		return nil, errors.Wrap(err, "error parsing vault initialization data file")
	}

	return &data, nil
}

func unsafeInitializationDataPath() string {
	if path, ok := os.LookupEnv("TOOL_FILE_STORAGE_PATH"); ok {
		return path
	}
	return "/opt/vault/data"
}
