// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package commands

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"syscall"

	"github.com/mitchellh/cli"
	"github.com/pkg/errors"
	"github.com/posener/complete"
	"github.com/simon-wenmouth/vault-tool/flags"
)

type ApproleExecCommand struct {
	UI                cli.Ui
	flagApprole       string
	flagApproleServer string
	flagBinary        string
	flagCaCert        string
	flagEnvPrefix     string
	flagEnvVariables  string // should be []string but flags.StringSliceVar is currently broken
	flagWrapToken     bool
}

func (c *ApproleExecCommand) Help() string {
	helpText := `
Usage: vault-tool approle exec [options]

      The approle exec command requests an approle wrapped token (from the
      approle server) and exec's the argument command with a the basic vault
      environment variables.

` + c.Flags().Help()

	return strings.TrimSpace(helpText)
}

func (c *ApproleExecCommand) Synopsis() string {
	return "Approle Commands"
}

func (c *ApproleExecCommand) Flags() *flags.FlagSets {
	set := flags.NewFlagSets(c.UI)

	f := set.NewFlagSet("Command Options")

	f.StringVar(&flags.StringVar{
		Name:    "approle",
		Default: "",
		EnvVar:  "TOOL_APPROLE",
		Target:  &c.flagApprole,
		Usage:   "The name of the approle to create the token against.",
	})

	f.StringVar(&flags.StringVar{
		Name:    "approle-server",
		Default: "http://127.0.0.1:8250",
		EnvVar:  "TOOL_APPROLE_SERVER",
		Target:  &c.flagApproleServer,
		Usage:   "Address of the `vault-tool approle server`.",
	})

	f.StringVar(&flags.StringVar{
		Name:    "binary",
		Default: "",
		EnvVar:  "TOOL_BINARY",
		Target:  &c.flagBinary,
		Usage:   "The fully qualified path to the executable.",
	})

	f.StringVar(&flags.StringVar{
		Name:    "ca-cert",
		Default: "",
		EnvVar:  "TOOL_CA_CERT",
		Target:  &c.flagCaCert,
		Usage:   "Path on the local disk into which a single PEM-encoded" +
				 "CA certificate will be written that the binary can use" +
			     "to verify the Vault server's SSL certificate.",
	})

	f.StringVar(&flags.StringVar{
		Name:    "env-prefix",
		Default: "",
		EnvVar:  "TOOL_ENV_PREFIX",
		Target:  &c.flagEnvPrefix,
		Usage:   "The string prefix to prepend to the VAULT_ADDR and " +
			     "VAULT_TOKEN environment variables passed to the binary.",
	})

	f.StringVar(&flags.StringVar{
		Name:   "env-variables",
		EnvVar: "TOOL_ENV_VARIABLES",
		Target: &c.flagEnvVariables,
		Usage:  "A comma-separated list of environment variable names that " +
			    "will be passed to the environment of the binary.",
	})

	f.BoolVar(&flags.BoolVar{
		Name:    "wrap-token",
		EnvVar:  "TOOL_WRAP_TOKEN",
		Default: true,
		Target:  &c.flagWrapToken,
		Usage:   "Indicate whether the vault token should be wrapped.",
	})

	return set
}

func (c *ApproleExecCommand) AutocompleteArgs() complete.Predictor {
	return complete.PredictNothing
}

func (c *ApproleExecCommand) AutocompleteFlags() complete.Flags {
	return c.Flags().Completions()
}

func (c *ApproleExecCommand) Run(args []string) int {
	f := c.Flags()

	var commandArgs = args
	var binaryArgs []string
	for i, arg := range args {
		if arg == "--" {
			commandArgs = args[:i]
			binaryArgs = args[i+1:]
			break
		}
	}

	if err := f.Parse(commandArgs); err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	if c.flagApprole == "" {
		c.UI.Error("You did not provide an approle.")
		return 1
	}

	if c.flagApproleServer == "" {
		c.UI.Error("You did not provide an approle server.")
		return 1
	}

	body, err := json.Marshal(&ApproleRequest{Approle: c.flagApprole, Wrap: c.flagWrapToken})
	if err != nil {
		c.UI.Error(errors.Wrap(err, "Error encoding request for approle server.").Error())
		return 1
	}
	c.UI.Info(fmt.Sprintf("wrap: %v", c.flagWrapToken))

	response, err := http.Post(fmt.Sprintf("%s/approle", c.flagApproleServer), "application/json", bytes.NewReader(body))
	if err != nil {
		c.UI.Error(errors.Wrap(err, "Error issuing request to approle server.").Error())
		return 1
	}

	contents, err := ioutil.ReadAll(response.Body)
	if err != nil {
		c.UI.Error(errors.Wrap(err, "Error reading response from approle server.").Error())
		return 1
	}

	if response.StatusCode != http.StatusOK {
		c.UI.Error(errors.Errorf("Error response from approle server: %s", string(contents)).Error())
		return 1
	}

	info := &ApproleResponse{}
	err = json.Unmarshal(contents, info)
	if err != nil {
		c.UI.Error(errors.Wrap(err, "Error unmarshalling response from approle server.").Error())
		return 1
	}

	if c.flagCaCert != "" {
		caCertificateFile, err := os.Create(c.flagCaCert)
		if err != nil {
			c.UI.Error(errors.Wrap(err, "Error opening CA certificate file for writing.").Error())
			return 1
		}
		defer caCertificateFile.Close()

		certificateWriter := bufio.NewWriter(caCertificateFile)
		n, err := fmt.Fprint(certificateWriter, info.VaultCaCert)
		if err == nil {
			c.UI.Info(fmt.Sprintf("ca-cert: %s (%d bytes)", c.flagCaCert, n))
		} else {
			c.UI.Info(fmt.Sprintf("ca-cert: %+v", err))
		}
		certificateWriter.Flush()
	}

	_, err = os.Stat(c.flagBinary)
	if err != nil {
		c.UI.Error(errors.Wrap(err, fmt.Sprintf("binary executable not found: %s", c.flagBinary)).Error())
		return 1
	}

	var binaryVars []string
	for _, key := range []string{"HOME", "USER", "PATH", "TERM", "SHLVL"} {
		if val := os.Getenv(key); val != "" {
			binaryVars = append(binaryVars, fmt.Sprintf("%s=%s", key, val))
		}
	}

	envVariables := strings.Split(c.flagEnvVariables, ",")
	for _, key := range envVariables {
		if val := os.Getenv(key); val != "" {
			binaryVars = append(binaryVars, fmt.Sprintf("%s=%s", key, val))
		}
	}

	envPrefix := c.flagEnvPrefix
	if envPrefix != "" && !strings.HasSuffix(envPrefix, "_") {
		envPrefix = envPrefix + "_"
	}

	var token string
	if c.flagWrapToken {
		token = info.WrapInfo.Token
	} else {
		token = info.Auth.ClientToken
	}

	binaryVars = append(binaryVars, fmt.Sprintf("%sVAULT_ADDR=%s", envPrefix, info.VaultAddr))
	binaryVars = append(binaryVars, fmt.Sprintf("%sVAULT_TOKEN=%s", envPrefix, token))
	if c.flagCaCert != "" {
		// by design: no need to alias the ca certificate
		binaryVars = append(binaryVars, fmt.Sprintf("VAULT_CA_CERT=%s", c.flagCaCert))
	}

	c.UI.Info(fmt.Sprintf("binary path: %v", c.flagBinary))
	c.UI.Info(fmt.Sprintf("binary args: %v", strings.Join(binaryArgs, " ")))
	c.UI.Info(fmt.Sprintf("binary vars: %v", strings.Join(binaryVars, " ")))

	err = syscall.Exec(c.flagBinary, binaryArgs, binaryVars)
	if err != nil {
		c.UI.Error(errors.Wrap(err, "failed to exec binary").Error())
		return 1
	}

	return 0 // but really not ... because the exec succeeded
}
